# Revolutions Pack Theme Editor


This pompously named _editor_ has barely gotten to the viewer stage. Things happened, development stopped,and it never was resumed. So one can only see theme elements, change their appearance in the viewer, but cannot add/modify/delete theme elements.

![](ss_rpte10013.png)

What is Revolutions Pack actually? It is a revolutionary (duh!) concept that natively skins Windows® 98/ME, unlike Stardock's Window Blinds. Developed entirely by a single bright person nicknamed **Tihiy** _(the silent one)_, it started as **LameSkin**, evolved to **UberSkin**, then finally reached the **Revolutions Pack** stage. Last known version is **9.7.2**, while the sources - extremely hard if not impossible to find now - have some of the files versioned as **10.0.0**.

While the Windows® 9x line (95/98/98SE/ME) has long overcome its usefulness in modern computing hobbyists may still enjoy installing and tweaking these OS versions either on bare metal or in virtual machines. For them this tool may prove to be at least interesting if not a debugging help when they work on creating/modifying RP9 themes.

However, I'm not sure how good this tool performs under a vanilla 9x system considering I used to have a heavily upgraded 98SE system while developing it. The tool worked fine in Windows® XP too, back when I tested it. That could be an alternative for checking themes in case it doesn't run - correctly or at all - on a plain or not sufficiently updated 9x system.

Fixing or improving anything now is extremely difficult, impossible even, considering I'm now running Linux and this tool has major problems under my older version of WINE (locks up when switching themes, doesn't display categories etc). Hopefully it works for you as is.

Enjoy!

**© Drugwash, 2014-2023**
