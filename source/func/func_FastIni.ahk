;==VERSIONING==>1.1
;About:	A series of ini-reading functions that fill in some gaps
;Compat:	B1.0.48.05,L1.1.13.0
;Depend:	updates.ahk
;Rqrmts:
;Date:	2014.04.25 22:27
;Version:	1.0
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash
;==VERSIONING==<

#include *i updates.ahk
; Uncomment the block below for a working example (requires Win98SE/ME and Revolutions Pack)
/*
inifile := "C:\WINDOWS\Resources\Themes\Windows XP\skin.ini"
i := IniReadSectionNames(inifile, buf)
msgbox, %i% sections:`n%buf%
idx=0
t=
s := IniFindSubSections(inifile, "StartButton", buf)
msgbox, %s% subsections:`n%buf%
Loop, Parse, buf, `n
	if InStr(A_LoopField, "StartButton")
	{
	idx++
	k := IniReadSection(inifile, A_LoopField, buf2)
	i%idx% := A_LoopField " : (" k " keys)`n" buf2
	t .= i%idx% "`n`n"
	}
UDLI := DllCall("GetUserDefaultLangID", "UShort")
SDLI := DllCall("GetSystemDefaultLangID", "UShort")
msgbox, User Default LangID: %UDLI%`nSystem Default LangID: %SDLI%`n`n%t%
return
*/
;================================================================
IniFindSubSections(inifile, section, ByRef buf, ch=0xA)
;================================================================
{
buf := "", idx := 0
IniReadSectionNames(inifile, buf1, ch)
Loop, Parse, buf1, % Chr(ch)
	if InStr(A_LoopField, section)=1
		{
		idx++
		buf .= A_LoopField Chr(ch)
		}
StringTrimRight, buf, buf, 1
VarSetCapacity(buf1, 0)
return idx
}
;================================================================
IniReadSectionNames(inifile, ByRef buf, ch=0xA)
;================================================================
{
Global
Static sz, st, hF
if !hF
	hF := DllCall("GetProcAddress"
			, Ptr, DllCall("GetModuleHandle" AW
					, "Str", "kernel32.dll", Ptr)
			, AStr, "GetPrivateProfileSectionNames" AW, Ptr)
sz=32767
VarSetCapacity(buf, sz, 0)	; max profile section size=32767
st := DllCall(hF
	, Ptr, &buf
	, "UInt", sz
	, "Str", inifile)
return IniFixBuf(buf, st, ch) - (A_IsUnicode!=TRUE)
}
;================================================================
IniReadSection(inifile, section, ByRef buf, ch=0xA)
;================================================================
{
Global
Static sz, st, hF
if !hF
	hF := DllCall("GetProcAddress"
			, Ptr, DllCall("GetModuleHandle" AW
					, "Str", "kernel32.dll", Ptr)
			, AStr, "GetPrivateProfileSection" AW, Ptr)
sz=32767
VarSetCapacity(buf, sz, 0)	; max profile section size=32767
st := DllCall(hF
	, "Str", section
	, Ptr, &buf
	, "UInt", sz
	, "Str", inifile)
return IniFixBuf(buf, st, ch)
}
;================================================================
IniFixBuf(ByRef buf, sz, ch=0xA)
;================================================================
{
Global
Static hF1, idx, i
if !hF1
	hF1 := DllCall("GetProcAddress"
			, Ptr, DllCall("GetModuleHandle" AW
					, "Str", "msvcrt.dll", Ptr)
			, AStr, (A_IsUnicode ? "wcschr" : "strchr"), Ptr)
aaw := 2**(A_IsUnicode=TRUE)
i := &buf-aaw
idx=0
Loop
	{
	i := DllCall(hF1, Ptr, i+aaw, "UChar", 0, "CDecl")
	if (i - &buf >= sz*aaw)
		break
	NumPut(ch, i+0, 0, "UChar")
	idx++
	}
VarSetCapacity(buf, -1)
return idx
}
