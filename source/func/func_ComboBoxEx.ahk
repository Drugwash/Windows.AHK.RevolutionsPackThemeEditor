;==VERSIONING==>1.1
;About:
;Compat:	B1.0.48.05
;Depend:
;Rqrmts:
;Date:	2014.04.20 17:54
;Version:	1.0
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash
;==VERSIONING==<

; CBS_SIMPLE=0x1 CBS_DROPDOWN=0x2 CBS_DROPDOWNLIST=0x3
; CBS_OWNERDRAWFIXED=0x10 CBS_OWNERDRAWVARIABLE=0x20 CBS_AUTOHSCROLL=0x40
; CBS_OEMCONVERT=0x80  CBS_SORT=0x100 CBS_HASSTRINGS=0x200 CBS_NOINTEGRALHEIGHT=0x400
; CBS_DISABLENOSCROLL=0x800 CBS_UPPERCASE=0x2000 CBS_LOWERCASE=0x4000
; CBES_EX_NOEDITIMAGE=0x1 CBES_EX_NOEDITIMAGEINDENT=0x2 CBES_EX_PATHWORDBREAKPROC=0x4 (NT only)
;CBES_EX_NOSIZELIMIT=0x8 CBES_EX_CASESENSITIVE=0x10 CBES_EX_TEXTENDELLIPSIS=0x20 (Vista+)
;================================================================
CBE_Create(g="1", t="", c="0|0|100|21", s="0x2", e="0")
;================================================================
{
Static
Static init=0, mid=190766, cs="xywh"
Global CBE_onh
if !init
	{
	sz=8
	VarSetCapacity(icex, sz, 0)		; INITCOMMONCONTROLSEX
	NumPut(sz, icex, 0, "UInt")
	NumPut(0x200, icex, 4, "UInt")	; ICC_USEREX_CLASSES
	if !init := DllCall("comctl32\InitCommonControlsEx", "UInt", &icex)
		{
		msgbox, Error in %A_ThisFunc%():`nCannot initiate Common Controls.
		Return 0
		}
	if (CBE_cnh := OnMessage(0x4E, "CBE_notif") != "CBE_notif")
		CBE_onh := RegisterCallback(CBE_cnh)
	}

StringSplit, c, c, |
Loop, Parse, cs
	%A_LoopField% := c%A_Index% ? c%A_Index% : "0"
if !(hGUI := GetHwnd(g))
	{
	msgbox, Error in %A_ThisFunc%():`nInvalid GUI count/handle: %hGUI%.
	Return 0
	}
if !hwnd := DllCall("CreateWindowEx"
		, "UInt", e
		, "Str", "ComboBoxEx32"
		, "Str", ""
		, "UInt", 0x50000000|s	; WS_CHILD WS_VISIBLE WS_BORDER CBS_DROPDOWN
		, "Int", x
		, "Int", y
		, "UInt", w
		, "UInt", h
		, "UInt", hGUI			; hWndParent
		, "UInt", mid			; hMenu
		, "UInt", 0			; hInstance
		, "UInt", 0)			; lpParam
	{
;	ErrorLevel := DllCall("GetLastError")
	msgbox, % A_ThisFunc "() failed with error " DllCall("GetLastError")
	Return 0
	}
;DllCall("SetWindowSubclass", "UInt", hwnd, "UInt", RegisterCallback("CBE_keyproc", "", 6, 0), "UInt", 0, "UInt", 0)
if t
	{
	Loop, Parse, t, |
		CBE_Add(hwnd, "tx" A_LoopField)
	}
return hwnd
}
;================================================================
; CBEIF_DI_SETITEM=0x10000000 CBEIF_IMAGE=0x2 CBEIF_INDENT=0x10 CBEIF_LPARAM=0x20
; CBEIF_OVERLAY=0x8 CBEIF_SELECTEDIMAGE=0x4 CBEIF_TEXT=0x1 
CBE_Add(hwnd, p="id-1|tx")
;================================================================
{
Loop, Parse, p, |
	{
	i := SubStr(A_LoopField,1,2)
	%i% := SubStr(A_LoopField,3)
	}
mk := (tx ? 0x1 : 0)|(ii ? 0x2: 0)|(si ? 0x4 : 0)|(oi ? 0x8 : 0)|(in ? 0x10 : 0)|(lP ? 0x20 : 0)|(ds ? 0x10000000 : 0)
VarSetCapacity(CBEI, 36, 0)		; COMBOBOXEXITEM
NumPut(mk, CBEI, 0, "UInt")		; mask
NumPut(id, CBEI, 4, "Int")		; iItem
NumPut(&tx, CBEI, 8, "UInt")		; pszText
;NumPut(sz, CBEI, 12, "UInt")	; cchTextMax
NumPut(ii-1, CBEI, 16, "Int")		; iImage
NumPut(si-1, CBEI, 20, "Int")		; iSelectedImage
NumPut(oi, CBEI, 24, "Int")		; iOverlay
NumPut(in, CBEI, 28, "Int")		; iIndent
NumPut(lP, CBEI, 32, "UInt")		; lParam
return 1+DllCall("SendMessage"
			, "UInt", hwnd
			, "UInt", (A_OSType=WIN32_NT ? 0x411 : 0x401)
			, "UInt", 0
			, "UInt", &CBEI)	; CBEM_INSERTITEMA/W
}
;================================================================
CBE_Set(hwnd, p)
;================================================================
{
Loop, Parse, p, |
	{
	StringLeft, p1, A_LoopField, 2
	StringTrimLeft, p2, A_LoopField, 2
	if p1=IL
		DllCall("SendMessage", "UInt", hwnd, "UInt", 0x402, "UInt", 0, "UInt", p2)	; CBEM_SETIMAGELIST
	}
}
;================================================================
CBE_Get(hwnd, id="-1", p="tx")
;================================================================
{
i := "tx,ii,si,oi,in,lP"
Loop, Parse, i, CSV
	if (p=A_LoopField)
		mk := 2**(A_Index-1)
sz=260
VarSetCapacity(CBEI, 36, 0)		; COMBOBOXEXITEM
VarSetCapacity(text, sz, 0)		; text buffer
NumPut(mk, CBEI, 0, "UInt")		; mask
NumPut(id, CBEI, 4, "Int")		; iItem
NumPut(&text, CBEI, 8, "UInt")	; pszText
NumPut(sz, CBEI, 12, "UInt")		; cchTextMax
DllCall("SendMessage"
	, "UInt", hwnd
	, "UInt", (A_OSType=WIN32_NT ? 0x40D : 0x404)
	, "UInt", 0
	, "UInt", &CBEI)			; CBEM_GETITEMA/W
if p=tx
	{
	b := NumGet(CBEI, 8, "UInt")
	if (b != &text)
		DllCall("lstrcpy", "UInt", &text, "UInt", b)
	VarSetCapacity(text, -1)
	r := text
	}
return r
}
;============ Needs fixed ============================================
CBE_notif(wP, lP, msg, hwnd)
;================================================================
{
Static mid=190766
Global CBE_onh
if CBE_onh
	DllCall(CBE_onh, "UInt", wP, "UInt", lP, "UInt", msg, "UInt", hwnd)
SetFormat, Integer, Hex
d := NumGet(lP+0, 4, "UInt")					; same with wP
if (d != mid)
	return
h := NumGet(lP+0, 0, "UInt")					; control handle
c := NumGet(lP+0, 8, "UInt")					; msg code
f := NumGet(lP+0, 12, "UInt")				; fChanged
if (c=0xFFFFFCDB OR c=0xFFFFFCDA)			; CBEN_ENDEDITA / CBEN_ENDEDITW
	{
	VarSetCapacity(text, 260, 0)				; text buffer
	DllCall("lstrcpy", "UInt", &text, "UInt", lP+20)
	VarSetCapacity(text, -1)
	x := DllCall("lstrlen", "UInt", &text)
	i := NumGet(lP+0, 16, "UInt")				; selection index (zero-based)
	w := NumGet(lP+0, 280, "UInt")			; CBEMAXSTRLEN=260
	if w in 1,2,4							; CBENF_DROPDOWN
		{
		r := CBE_Get(h, i)
		}
;	else if w=1	; CBENF_KILLFOCUS
;	else if w=2	; CBENF_RETURN
;	else if w=3	; CBENF_ESCAPE
;GuiControl,, Edit2, h=%h%`nc=%c%`nd=%d%`nf=%f%`ni=%i%`nr=%r%`nw=%w%`nx=%x%`ntext=%text%`nwP=%wP%
	}
return 0
}
;================================================================
CBE_keyproc(hwnd, msg, wP, lP, Uptr, Dptr)
;================================================================
{
r := DllCall("DefSubclassProc", "UInt", hwnd, "UInt", msg, "UInt", wP, "UInt", lP)
;GuiControl,, Edit2, msg=%msg% wP=%wP%
if msg=0x87								; WM_GETDLGCODE
	if (lP && NumGet(lP+0, 8, "UInt")=0x100)	; WM_KEYDOWN
		if (wP & 0xFF) in 0xD,0x1B			; VK_RETURN VK_ESCAPE
			{
			r |= 0x4
			; process keypress
			}
return r
}

;#include func_GetHwnd.ahk
