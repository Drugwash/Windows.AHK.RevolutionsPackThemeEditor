;==VERSIONING==>1.1
;About:	
;Compat:	B1.0.48.05,L1.1.14.3
;Depend:	updates.ahk
;Rqrmts:	DeleteObject() on control bitmap and created brush
;Date:	2014.05.16 19:28
;Version:	1.1
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash 2012-2014
;==VERSIONING==<
;================================================================
Brush(stl="1", chp="", dib="0")
;================================================================
{
Global Ptr, AW
ofi := A_FormatInteger
SetFormat, Integer, D
stl+=0
if stl=3
	{
	dib := dib<>"" ? dib : "0x0"
	StringSplit, s, dib, x
	IfExist, %chp%
		hnd := DllCall("LoadImage" AW, Ptr, 0, "Str", chp, "UInt", 0, "UInt", s1, "UInt", s2, "UInt", 0x10)	; LR_LOADFROMFILE
	}
else if stl in 5,6
	{
	clr := dib ? 1 : 0
	hnd := chp
	}
else if stl in 0,2
	{
	clr := chp
	if dib between 0 and 7	; hatch styles
		hnd := dib
	}
else
	{
	clr := dib
	hnd := chp
	}
VarSetCapacity(LB, 12, 0)				; LOGBRUSH struct
NumPut((stl<>"" ? stl : "1"), LB, 0, "UInt")	; lbStyle
NumPut((clr<>"" ? clr : "0"), LB, 4, "UInt")	; lbColor
NumPut((hnd ? hnd : "0"), LB, 8, "UInt")		; lbHatch
SetFormat, Integer, %ofi%
return DllCall("CreateBrushIndirect", Ptr, &LB)
}
;================================================================
checkBrush(sz="16", c1="0xC0C0C0", c2="")	; sz=size of a color square (will be 2x2 squares)
;================================================================
{
Global Ptr
sz := sz ? sz : 16
hDC := DllCall("GetDC", Ptr, 0, Ptr)
hDCtmp := DllCall("CreateCompatibleDC", Ptr, hDC, Ptr)
hBm := DllCall("CreateCompatibleBitmap" , Ptr, hDC, "Int", 2*sz, "Int", 2*sz, Ptr)
hBo := DllCall("SelectObject", Ptr, hDCtmp, Ptr, hBm)
DllCall("ReleaseDC", Ptr, 0, Ptr, hDC)
c2 := c2!="" ? c2 : c1
b1 := DllCall("CreateSolidBrush", "UInt", SwColor(c1))
b2 := DllCall("CreateSolidBrush", "UInt", SwColor(c2))
Loop, 4
	{
	cb := (A_Index=2 OR A_Index=3 ? b2 : b1)
;	DllCall("UnrealizeObject", Ptr, cb), DllCall("SetBrushOrgEx", Ptr, hDCtmp, "Int", 0, "Int", 0, Ptr, 0)
	x := sz*(A_Index-1-2*(A_Index>2)), y := sz*((A_Index-1)//2)
	hB%A_Index% := DllCall("SelectObject", Ptr, hDCtmp, Ptr, cb)
	DllCall("PatBlt", Ptr, hDCtmp, Int, x, Int, y, Int, sz, Int, sz, "UInt", 0xF00021)
	}
DllCall("SelectObject", Ptr, hDCtmp, Ptr, hB1)
DllCall("DeleteObject", Ptr, b1)
DllCall("DeleteObject", Ptr, b2)

VarSetCapacity(LB, 12, 0)	; LOGBRUSH struct
NumPut(3, LB, 0, "UInt")		; lbStyle BS_PATTERN
NumPut(hBm, LB, 8, Ptr)		; lbHatch
hBr := DllCall("CreateBrushIndirect", Ptr, &LB)
DllCall("SelectObject", Ptr, hDCtmp, Ptr, hBo)
DllCall("DeleteDC", Ptr, hDCtmp)
DllCall("DeleteObject", Ptr, hBm)
return hBr
}
;================================================================
setBrush(hCtrl, hBr)
;================================================================
{
Global Ptr, AW
ControlGetPos,,, cw, ch,, ahk_id %hCtrl%
ControlGet, st, Style,,, ahk_id %hCtrl%
if (st & 0x800000)		; if the control has WS_BORDER set,
	cw -= 2, ch -= 2	; subtract 2px on each dimension
hDC := DllCall("GetDC", Ptr, hCtrl, Ptr)
hDCtmp := DllCall("CreateCompatibleDC", Ptr, hDC, Ptr)
;DllCall("UnrealizeObject", Ptr, hBr), DllCall("SetBrushOrgEx", Ptr, hDC, "Int", 0, "Int", 0, Ptr, 0)
hBm := DllCall("CreateCompatibleBitmap" , Ptr, hDC, "UInt", cw, "UInt", ch, Ptr)
hBo := DllCall("SelectObject", Ptr, hDCtmp, Ptr, hBm)
hBro := DllCall("SelectObject", Ptr, hDCtmp, Ptr, hBr)
DllCall("PatBlt", Ptr, hDCtmp, "Int", 0, "Int", 0, "UInt", cw, "UInt", ch, "UInt", 0xF00021)
DllCall("SendMessage" AW, Ptr, hCtrl, "UInt", 0x172, "UInt", 0, Ptr, hBm)	; STM_SETIMAGE, IMAGE_BITMAP
DllCall("SelectObject", Ptr, hDCtmp, Ptr, hBro)	; don't delete brush here, let the app do it on exit
DllCall("SelectObject", Ptr, hDCtmp, Ptr, hBo)
DllCall("DeleteDC", Ptr, hDCtmp)
DllCall("ReleaseDC", Ptr, hCtrl, Ptr, hDC)
return hBm
}
;================================================================
/*
brush style:
BS_DIBPATTERN=5
BS_DIBPATTERNPT=6
BS_HATCHED=2
BS_HOLLOW=BS_NULL=1
BS_PATTERN=3
BS_SOLID=0

dib_style:
DIB_PAL_COLORS=1
DIB_RGB_COLORS=0

hatch style:
HS_BDIAGONAL=3
HS_BDIAGONAL1=7
HS_CROSS=4
HS_DIAGCROSS=5
HS_FDIAGONAL=2
HS_FDIAGONAL1=6
HS_HORIZONTAL=0
HS_VERTICAL=1

examples:
Brush()					; hollow brush
Brush(0, 0xFF0000)		; solid brush
Brush(2, 0xFF0000, 4)		; hatched brush, cross
Brush(3, GridFile, "20x20")	; pattern brush, 20x20px
Brush(5, hDIB, 1)			; pattern DIB brush, palette
*/
