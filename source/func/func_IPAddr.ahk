;==VERSIONING==>1.1
;About:
;Compat:	B1.0.48.05,L1.1.13.0
;Depend:	Common Controls 4.71+,updates.ahk
;Rqrmts:	IPAddr_Cleanup() on exit
;Date:	2014.04.27 04:43
;Version:	1.0
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash
;==VERSIONING==<

;================================================================
IPAddr_Create(g="1", a="", c="0|0|130|20", s="0")
;================================================================
{
Global Ptr, AW
Static init, cs="xywh"
if !init
	{
	sz=8
	VarSetCapacity(icex, sz, 0)		; INITCOMMONCONTROLSEX
	NumPut(sz, icex, 0, "UInt")
	NumPut(0x800, icex, 4, "UInt")	; ICC_INTERNET_CLASSES
	if !init := DllCall("comctl32\InitCommonControlsEx", Ptr, &icex)
		{
		msgbox, Error in %A_ThisFunc%():`nCannot initiate Common Controls.
		Return 0
		}
	}
if !(hGUI := GetHwnd(g))
	{
	msgbox, Error in %A_ThisFunc%():`nInvalid GUI count/handle: %hGUI%.
	Return 0
	}
StringSplit, c, c, |
Loop, Parse, cs
	%A_LoopField% := c%A_Index% ? c%A_Index% : "0"
if !hwnd := DllCall("CreateWindowEx" AW
		, Ptr, 0
		, "Str", "SysIPAddress32"
		, "Str", ""
		, "UInt", 0x50010000|s	; WS_CHILD WS_VISIBLE WS_TABSTOP
		, "Int", x
		, "Int", y
		, "UInt", w
		, "UInt", h
		, Ptr, hGUI			; hWndParent
		, Ptr, 0				; hMenu
		, Ptr, 0				; hInstance
		, Ptr, 0)				; lpParam
	{
	msgbox, % A_ThisFunc "() failed with error " DllCall("GetLastError")
	Return 0
	}
if a
	IPAddr_SetAddr(hwnd, a)
return hwnd
}
;================================================================
IPAddr_Color(g="1", a="", c="0|0|130|20", s="0")
;================================================================
{
Global Ptr, AW
StringSplit, c, c, |
c1 -= 3
c3 := (c3 != "" ? c3 : 130)
c4 := (c4 != "" ? c4 : 20)
c := c1 "|" c2 "|" c3 "|" c4
if !hwnd := IPAddr_Create(g, a, c, s)
	return
w := 8+3*((c3-20)/4)
DllCall("SetWindowLong" AW, Ptr, hwnd, "Int", -20, "UInt", 0)	; GWL_EXSTYLE
DllCall("SetWindowPos", Ptr, hwnd, Ptr, 0, "Int", 0, "Int", 0, "Int", 0, "Int", 0, "UInt", 0x27)
WinSet, Region, 3-0 W%w% H%c4%, ahk_id %hwnd%
return hwnd
}
;================================================================
IPAddr_Cleanup(hIP)
;================================================================
{
Global
return DllCall("DestroyWindow", Ptr, hIP)
}
;================================================================
IPAddr_Clear(hIP)
;================================================================
{
SendMessage, 0x464, 0, 0,, ahk_id %hIP%				; IPM_CLEARADDRESS
}
;================================================================
IPAddr_Focus(hIP, p)
;================================================================
{
SendMessage, 0x468, p, 0,, ahk_id %hIP%				; IPM_SETFOCUS
}
;================================================================
IPAddr_SetRange(hIP, p)	; use p as min-max|min-max|min-max|min-max
;================================================================
{
Global Ptr, AW
StringSplit, v, p, |
Loop, %v0%
	{
	StringSplit, r, v%A_Index%, -, %A_Space%
	i := r1 | r2<<8
	SendMessage, 0x467, A_Index-1, i,, ahk_id %hIP%	; IPM_SETRANGE
	r%A_Index% := ErrorLevel
	}
return r1 " " r2 " " r3 " " r4
}
;================================================================
IPAddr_SetAddr(hIP, p)
;================================================================
{
Global Ptr, PtrP, AW
if p between 0 and 0xFFFFFFFF
	SendMessage, 0x465, 0, p,, ahk_id %hIP%			; IPM_SETADDRESS
else
	{
	v1 := v2 := v3 := v4 := "0"
	StringReplace, p, p, %A_Space%, ., All
	StringReplace, p, p, :, ., All
	StringReplace, p, p, |, ., All
	i=0
	StringSplit, v, p, .
	Loop, %v0%
		i += (v%A_Index% << (8*(4-A_Index)))
	SendMessage, 0x465, 0, i,, ahk_id %hIP%			; IPM_SETADDRESS
	}
}
;================================================================
IPAddr_GetAddr(hIP, ByRef p, s=".")
;================================================================
{
p := ""
VarSetCapacity(b, 4, 0)
SendMessage, 0x466, 0, &b,, ahk_id %hIP%			; IPM_GETADDRESS
r := ErrorLevel
s := s!="" ? s : "."
Loop, 4
	p .= (*(&b+4-A_Index)) s
StringTrimRight, p, p, 1
return r
}
;================================================================
IPAddr_IsBlank(hIP)
;================================================================
{
SendMessage, 0x469, 0, 0,, ahk_id %hIP%				; IPM_ISBLANK
return ErrorLevel
}
