; DllGetVersion() wrapper v1.2
; � Drugwash September 2011-May 2014
; ByRef variables will get: version [major.minor.build] platform [Win32|WinNT] QFE version AND flags
; Returns 1 on Success or 0/-1/-2/-3 on failure. Also returns OCX where appropriate
;================================================================
DllGetVersion(cFile, ByRef ver, ByRef p, ByRef q, ByRef f)
;================================================================
{
Global Ptr, AW, AStr
Static bad := "uberskin,routetab,comctlv8,diactfrm,dbmsgnet,dbnmpntw,msctf.dll", lazy := "mtxoci,msorcl32"
GuiControl,, debug, %cFile%
Loop, Parse, bad, CSV
	if InStr(cFile, A_LoopField ".dll")
		return "bad skip"
Loop, Parse, lazy, CSV
	if InStr(cFile, A_LoopField ".dll")
		return "lazy skip"
VarSetCapacity(DVI, 32, 0)
NumPut(20, DVI, 0, "UInt")	; DLLVERSIONINFO size
if !a := DllCall("GetModuleHandle" AW, "Str", cFile)
	if !(l := a := DllCall("LoadLibrary" AW, "Str", cFile))
		return -1
if DllCall("GetProcAddress", Ptr, a, AStr, "DllRegisterServer")	; it's an OCX
	{
	if l, DllCall("FreeLibrary", Ptr, a)
	return "OCX"
	}
if !b := DllCall("GetProcAddress", Ptr, a, AStr, "DllGetVersion")
	{
	if l, DllCall("FreeLibrary", Ptr, a)
	return -2
	}
if c := DllCall(b, Ptr, &DVI)
	{
	if l, DllCall("FreeLibrary", Ptr, a)
	return -3
	}
if (d := NumGet(DVI, 4, "UInt") "." NumGet(DVI, 8, "UInt")) < 4.90
	{
	ver :=  d "." NumGet(DVI, 12, "UInt")
	p := (NumGet(DVI, 16, "UInt")="1" ? "Win32" : "WinNT")
	q := f := "no"
	}
else
	{
	NumPut(32, DVI, 0, "UInt")	; DLLVERSIONINFO2 size (WinME+)
	if !c := DllCall(b, Ptr, &DVI)
		{
		p := (NumGet(DVI, 16, "UInt")="1" ? "Win32" : "WinNT")
		f := NumGet(DVI, 20, "UInt")
		d := NumGet(DVI, 24, "Int64")
		q := d & 0xFFFF
		ver := (d >> 48) & 0xFFFF "." (d >> 32) & 0xFFFF "." (d >> 16) & 0xFFFF
		}
	}
return (l ? !DllCall("FreeLibrary", Ptr, a) : TRUE)
}
