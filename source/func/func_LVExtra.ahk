;==VERSIONING==>1.1
;About:
;Compat:	B1.0.48.05
;Depend:
;Rqrmts:
;Date:	2014.04.22 02:50
;Version:	1.0
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash
;==VERSIONING==<

;================================================================
LV_FindItem(lv, txt)
;================================================================
{
Global Ptr, AW
GuiControlGet, h, Hwnd, %lv%
VarSetCapacity(LVFI, 24+8*(A_PtrSize=8), 0)			; LVFINDINFO
NumPut(0x2, LVFI, 0, "UInt")							; flags LVFI_STRING
NumPut(&txt, LVFI, 4, Ptr)							; psz
return 1+DllCall("SendMessage" AW
			, Ptr, h
			, "UInt", 0x100D+70*(A_IsUnicode=TRUE)	; LVM_FINDITEMA/W
			, "UInt", 0xFFFFFFFF
			, Ptr, &LVFI)
}
