;==VERSIONING==>1.1
;About:
;Compat:	B1.0.48.05
;Depend:
;Rqrmts:
;Date:	2013.08.29 17:20
;Version:	1.0
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash
;==VERSIONING==<

; � Drugwash, August 2013
/* EXAMPLE
r := FindLang(0x419, "N")						; Name for specified LCID
r := FindLang("ro-RO", "L")						; LCID for specified ISO
r := FindLang("Spanish (Spain, Traditional Sort)", "L")	; LCID for specified name
r := FindLang("French (France)", "A")				; All, Name type (same as input if not specified)
r := FindLang("hi-IN", "ALP")						; All, LCID type, hi-IN preset
msgbox, Requested: %r%
return
*/

; N=language name, I=ISO639-2, L=LCID, A=All of input type; AP=input preselected
FindLang(in, t="N")
{
Static r := "NIL", strg := "|Afrikaans (South Africa)$af-ZA$0436|Albanian (Albania)$sq-AL$041C|Alsatian (France)$gsw-FR$0484|Amharic (Ethiopia)$am-ET$045E|Arabic (Algeria)$ar-DZ$1401|Arabic (Bahrain)$ar-BH$3C01|Arabic (Egypt)$ar-EG$0C01|Arabic (Iraq)$ar-IQ$0801|Arabic (Jordan)$ar-JO$2C01|Arabic (Kuwait)$ar-KW$3401|Arabic (Lebanon)$ar-LB$3001|Arabic (Libya)$ar-LY$1001|Arabic (Morocco)$ar-MA$1801|Arabic (Oman)$ar-OM$2001|Arabic (Qatar)$ar-QA$4001|Arabic (Saudi Arabia)$ar-SA$0401|Arabic (Syria)$ar-SY$2801|Arabic (Tunisia)$ar-TN$1C01|Arabic (U.A.E.)$ar-AE$3801|Arabic (Yemen)$ar-YE$2401|Armenian (Armenia)$hy-AM$042B|Assamese (India)$as-IN$044D|Azeri (Azerbaijan, Cyrillic)$az-AZ$082C|Azeri (Azerbaijan, Latin)$az-AZ$042C|Bangla (Bangladesh)$bn-BD$0845|Bangla (India)$bn-IN$0445|Bashkir (Russia)$ba-RU$046D|Basque (Basque)$eu-ES$042D|Belarusian (Belarus)$be-BY$0423|Bosnian ()$bs-$781A|Bosnian (Bosnia & Herzegovina, Cyrillic)$bs-BA$201A|Bosnian (Bosnia & Herzegovina, Latin)$bs-BA$141A|Breton (France)$br-FR$047E|Bulgarian (Bulgaria)$bg-BG$0402|Central Kurdish (Iraq)$ku-IQ$0492|Cherokee (Cherokee)$chr-Cher$045C|Catalan (Spain)$ca-ES$0403|Chinese (China)$zh-CN$0804|Chinese (Hong Kong SAR, PRC)$zh-HK$0C04|Chinese (Macao SAR)$zh-MO$1404|Chinese (Singapore)$zh-SG$1004|Chinese (Simplified)$zh-Hans-CN$0004|Chinese (Taiwan)$zh-TW$0404|Chinese (Traditional)$zh-Hant-CN$7C04|Corsican (France)$co-FR$0483|Croatian ()$hr-$001A|Croatian (Bosnia & Herzegovina, Latin)$hr-BA$101A|Croatian (Croatia)$hr-HR$041A|Czech (Czech Republic)$cs-CZ$0405|Danish (Denmark)$da-DK$0406|Dari (Afghanistan)$prs-AF$048C|Divehi (Maldives)$dv-MV$0465|Dutch (Belgium)$nl-BE$0813|Dutch (Netherlands)$nl-NL$0413|English (Australia)$en-AU$0C09|English (Belize)$en-BZ$2809|English (Canada)$en-CA$1009|English (Caribbean)$en-029$2409|English (India)$en-IN$4009|English (Ireland)$en-IE$1809|English (Ireland)$en-IE$1809|English (Jamaica)$en-JM$2009|English (Malaysia)$en-MY$4409|English (New Zealand)$en-NZ$1409|English (Philippines)$en-PH$3409|English (Singapore)$en-SG$4809|English (South Africa)$en-ZA$1c09|English (Trinidad & Tobago)$en-TT$2C09|English (United Kingdom)$en-GB$0809|English (United States)$en-US$0409|English (Zimbabwe)$en-ZW$3009|Estonian (Estonia)$et-EE$0425|Faroese (Faroe Islands)$fo-FO$0438|Filipino (Philippines)$fil-PH$0464|Finnish (Finland)$fi-FI$040B|French (Belgium)$fr-BE$080c|French (Canada)$fr-CA$0C0C|French (France)$fr-FR$040c|French (Luxembourg)$fr-LU$140C|French (Monaco)$fr-MC$180C|French (Switzerland)$fr-CH$100C|Frisian (Netherlands)$fy-NL$0462|Galician (Spain)$gl-ES$0456|Georgian (Georgia)$ka-GE$0437|German (Austria)$de-AT$0C07|German (Germany)$de-DE$0407|German (Liechtenstein)$de-LI$1407|German (Luxembourg)$de-LU$1007|German (Switzerland)$de-CH$0807|Greek (Greece)$el-GR$0408|Greenlandic (Greenland)$kl-GL$046F|Gujarati (India)$gu-IN$0447|Hausa (Nigeria)$ha-NG$0468|Hawaiian (United States)$haw-US$0475|Hebrew (Israel)$he-IL$040D|Hindi (India)$hi-IN$0439|Hungarian (Hungary)$hu-HU$040E|Icelandic (Iceland)$is-IS$040F|Igbo (Nigeria)$ig-NG$0470|Indonesian (Indonesia)$id-ID$0421|Inuktitut (Canada)$iu-CA$085D|Inuktitut (Canada)$iu-CA$045D|Irish (Ireland)$ga-IE$083C|isiXhosa (South Africa)$xh-ZA$0434|isiZulu (South Africa)$zu-ZA$0435|Italian (Italy)$it-IT$0410|Italian (Switzerland)$it-CH$0810|Japanese (Japan)$ja-JP$0411|Kannada (India)$kn-IN$044B|Kannada ()$kn-reserved$044B|Kazakh (Kazakhstan)$kk-KZ$043F|Khmer (Cambodia)$kh-KH$0453|K'iche (Guatemala)$qut-GT$0486|Kinyarwanda (Rwanda)$rw-RW$0487|Konkani (India)$kok-IN$0457|Korean (Korea)$ko-KR$0412|Kyrgyz (Kyrgyzstan)$ky-KG$0440|Lao (Lao PDR)$lo-LA$0454|Latvian (Latvia)$lv-LV$0426|Lithuanian (Lithuanian)$lt-LT$0427|Lower Sorbian (Germany)$dsb-DE$082E|Luxembourgish (Luxembourg)$lb-LU$046E|Macedonian (Macedonia, FYROM)$mk-MK$042F|Malay (Brunei Darassalam)$ms-BN$083E|Malay (Malaysia)$ms-MY$043e|Malayalam (India)$ml-IN$044C|Maltese (Malta)$mt-MT$043A| (Malta)$reserved-MT$043A|Maori (New Zealand)$mi-NZ$0481|Mapudungun (Chile)$arn-CL$047A|Marathi (India)$mr-IN$044E|Mohawk (Canada)$moh-CA$047C|Mongolian (Mongolia, Cyrillic)$mn-MN$0450|Mongolian (Mongolia, Mong)$mn-MN$0850|Nepali (Nepal)$ne-NP$0461|Nepali (India)$ne-IN$0461|Norwegian (Bokma*l, Norway)$nb-NO$0414|Norwegian (Nynorsk, Norway)$nn-NO$0814|Occitan (France)$oc-FR$0482|Oriya (India)$or-IN$0448|Pashto (Afghanistan)$ps-AF$0463|Persian (Iran)$fa-IR$0429|Polish (Poland)$pl-PL$0415|Portuguese (Brazil)$pt-BR$0416|Portuguese (Portugal)$pt-PT$0816|Pular (Senegal)$ff-SN$0867|Punjabi (India, Gurmukhi script)$pa-IN$0446|Punjabi (Pakistan, Arabic script)$pa-PK$0846|Quechua (Bolivia)$quz-BO$046B|Quechua (Ecuador)$quz-EC$086B|Quechua (Peru)$quz-PE$0C6B|Romanian (Moldova)$ro-MD$0818|Romanian (Romania)$ro-RO$0418|Romansh (Switzerland)$rm-CH$0417|Russian (Russia)$ru-RU$0419|Sakha (Russia)$sah-RU$0485|Sami (Inari, Finland)$smn-FI$243B|Sami (Lule, Norway)$smj-NO$103B|Sami (Lule, Sweden)$smj-SE$143B|Sami (Northern, Finland)$se-FI$0C3B|Sami (Northern, Norway)$se-NO$043B|Sami (Northern, Sweden)$se-SE$083B|Sami (Skolt, Finland)$sms-FI$203B|Sami (Southern, Norway)$sma-NO$183B|Sami (Southern, Sweden)$sma-SE$1C3B|Sanskrit (India)$sa-IN$044F|Serbian ()$sr-$7C1A|Serbian (Bosnia & Herzegovina, Cyrillic)$sr-Cyrl-BA$1C1A|Serbian (Bosnia & Herzegovina, Latin)$sr-Latn-BA$181A|Serbian (Croatia)$sr-HR$181A|Serbian (Serbia & Montenegro, Former, Cyrillic)$sr-Cyrl-CS$0C1A|Serbian (Serbia & Montenegro, Former, Latin)$sr-Latn-CS$081A|Sesotho sa Leboa (South Africa)$nso-ZA$046C|Setswana / Tswana (Botswana)$tn-BW$0832|Setswana / Tswana (South Africa)$tn-ZA$0432| ()$reserved-reserved$0432| ()$reserved-reserved$0459|Sindhi (Pakistan)$sd-PK$0859|Sinhala (Sri Lanka)$si-LK$045B|Slovak (Slovakia)$sk-SK$041B|Slovenian (Slovenia)$sl-SI$0424|Spanish (Argentina)$es-AR$2C0A|Spanish (Bolivia)$es-BO$400A|Spanish (Chile)$es-CL$340A|Spanish (Colombia)$es-CO$240A|Spanish (Costa Rica)$es-CR$140A|Spanish (Dominican Republic)$es-DO$1C0A|Spanish (Ecuador)$es-EC$300A|Spanish (El Salvador)$es-SV$440A|Spanish (Guatemala)$es-GT$100A|Spanish (Honduras)$es-HN$480A|Spanish (Mexico)$es-MX$080A|Spanish (Nicaragua)$es-NI$4C0A|Spanish (Panama)$es-PA$180A|Spanish (Paraguay)$es-PY$3C0A|Spanish (Peru)$es-PE$280A|Spanish (Puerto Rico)$es-PR$500A|Spanish (Spain, Modern Sort)$es-ES$0C0A|Spanish (Spain, Traditional Sort)$es-ES$040A|Spanish (United States)$es-US$540A|Spanish (Uruguay)$es-UY$380A|Spanish (Venezuela)$es-VE$200A|Swahili (Kenya)$sw-KE$0441|Swedish (Finland)$sv-FI$081D|Swedish (Sweden)$sv-SE$041D|Swedish (Sweden)$sv-SE$041D|Syriac (Syria)$syr-SY$045A|Tajik (Tajikistan, Cyrillic)$tg-TJ$0428|Tamazight (Algeria, Latin)$tzm-DZ$085F|Tamil (India)$ta-IN$0449|Tamil (Sri Lanka)$ta-LK$0849|Tatar (Russia)$tt-RU$0444|Telugu (India)$te-IN$044A|Thai (Thailand)$th-TH$041E|Tibetan (PRC)$bo-CN$0451|Tigrinya (Eritrea)$ti-ER$0873|Tigrinya (Ethiopia)$ti-ET$0473| ()$reserved-reserved$0873|Turkish (Turkey)$tr-TR$041F|Turkmen (Turkmenistan)$tk-TM$0442|Ukrainian (Ukraine)$uk-UA$0422|Upper Sorbian (Germany)$hsb-DE$042E|Urdu ()$ur-reserved$0820|Urdu (Pakistan)$ur-PK$0420|Uyghur (PRC)$ug-CN$0480|Uzbek (Uzbekistan, Cyrillic)$uz-UZ$0843|Uzbek (Uzbekistan, Latin)$uz-UZ$0443|Valencian (Valencia)$ca-ES-Valencia$0803|Vietnamese (Vietnam)$vi-VN$042A|Welsh (United Kingdom)$cy-GB$0452|Wolof (Senegal)$wo-SN$0488|Yi (PRC)$ii-CN$0478|Yoruba (Nigeria)$yo-NG$046A|"
ofi := A_FormatInteger
if in is xdigit
	{
	SetFormat, Integer, H		; account for possible decimal input
	in+=0					; transform to hex format
	SetFormat, Integer, D
	StringTrimLeft, in, in, 2		; remove '0x' from string
	in := SubStr("0000" in, -3)	; fill with leading zeroes to 4 char length
	if !i := InStr(strg, in)
		return, SetFormat, Integer, %ofi%
	j := RegExMatch(strg, "i)\|[^\$]*\$[^\$]*\$" in "?\|", sub)
	type := "L"
	}
else if InStr(in, "-")
	{
	if !i := InStr(strg, in)
		return, SetFormat, Integer, %ofi%
	j := RegExMatch(strg, "i)\|[^\$]*\$" in "\$.*?\|", sub)
	type := "I"
	}
else
	{
	if !i := InStr(strg, in)
		return, SetFormat, Integer, %ofi%
	j := RegExMatch(strg, "i)\|\Q" in "\E[^\$]*\$.*?\|", sub)
	type := "N"
	}
StringTrimLeft, sub, sub, 1		; remove starting pipe
StringTrimRight, sub, sub, 1		; remove trailing pipe
StringSplit, s, sub, $
s3 := "0x" s3					; make LCID a full hex string
if InStr(t, "A")					; check if we want the whole list
	{
	if InStr(t, "P")
		p := TRUE
	Loop, Parse, r				; find which type to add to list
		if InStr(t, A_LoopField)
			x := A_Index
	if !x						; if user didn't specify a type
		x := InStr(t, type)		; return same type as input
	SetFormat, Integer, D
	list := ""
	Loop, Parse, strg, |
		{
		if !A_LoopField
			continue
		StringSplit, s, A_LoopField, $
		if (!s1 OR !s2 OR InStr(s2, "reserved"))	; skip unsettled issues
			continue
		list .= s%x% "|"
		if p & (in=s1 OR in=s2 OR in=s3)
			list .= "|"
		}
	if (p & InStr(list, "||") != StrLen(list)-1)		; don't remove trailing pipe
		StringTrimRight, list, list, 1			; if last item is preselected
	}
SetFormat, Integer, %ofi%
;msgbox, Position is %j% for %in% (%type%)`nSubstring: %sub%`ns1=%s1%`ns2=%s2%`ns3=%s3%
return (t="N" ? s1 : t="I" ? s2 : t="L" ? s3 : InStr(t, "A") ? list : "wrong parameter: " t)
}
