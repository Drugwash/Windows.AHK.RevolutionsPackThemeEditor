;==VERSIONING==>1.1
;About:
;Compat:	B1.0.48.05,L1.1.13.0
;Depend:	updates.ahk
;Rqrmts:
;Date:	2014.05.02 02:20
;Version:	2.0
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash
;==VERSIONING==<

; RBS_TOOLTIPS=0x100 RBS_VARHEIGHT=0x200 RBS_BANDBORDERS=0x400 RBS_FIXEDORDER=0x800
; RBS_REGISTERDROP=0x1000 RBS_AUTOSIZE=0x2000 RBS_VERTICALGRIPPER=0x4000
; RBS_DBLCLKTOGGLE=0x8000

; CCS_TOP=0x1 CCS_NOMOVEY=0x2 CCS_BOTTOM=0x3 CCS_NORESIZE=0x4
; CCS_NOPARENTALIGN=0x8 CCS_ADJUSTABLE=0x20 CCS_NODIVIDER=0x40 CCS_VERT=0x80
; CCS_LEFT=CCS_VERT|CCS_TOP CCS_RIGHT=CCS_VERT|CCS_BOTTOM
; CCS_NOMOVEX=CCS_VERT|CCS_NOMOVEY

;================================================================
RB_Create(g="1", p="pT s0x2740", hIL="0")
;================================================================
{
Global Ptr, AW
Static init=0, idx=1, pos="TBLR", ps="0x1|0x3|0x81|0x83"
if !init
	{
	sz=8
	VarSetCapacity(icex, sz, 0)		; INITCOMMONCONTROLSEX
	NumPut(sz, icex, 0, "UInt")
	NumPut(0x404, icex, 4, "UInt")	; ICC_COOL_CLASSES/BAR_CLASSES
	if !init := DllCall("comctl32\InitCommonControlsEx", Ptr, &icex)
		{
		msgbox, Error in %A_ThisFunc%():`nCannot initiate Common Controls.
		Return 0
		}
	}
if !(hGUI := GetHwnd(g))
	{
	msgbox, Error in %A_ThisFunc%():`nInvalid GUI count/handle: %hGUI%.
	Return 0
	}
style := 0x56010000			; WS_CHILD/VISIBLE/CLIPSIBLINGS/CLIPCHILDREN/TABSTOP
Loop, Parse, p, %A_Space%, %A_Space%%A_Tab%
	{
	StringLeft, p1, A_LoopField, 1
	StringTrimLeft, p2, A_LoopField, 1
	if p1=s
		style |= p2
	if p1=p
		Loop, Parse, ps, |
			if (A_Index=InStr(pos, p2))
				style |= A_LoopField
	}
if !hwnd := DllCall("CreateWindowEx" AW
		, "UInt", 0x80		; WS_EX_TOOLWINDOW (TRANSPARENT=0x20, TOPMOST=0x8)
		, "Str", "ReBarWindow32"
		, "Str", ""
		, "UInt", style
		, "Int", 0
		, "Int", 0
		, "UInt", 0
		, "UInt", 0
		, "UInt", hGUI		; hWndParent
		, "UInt", 0		; hMenu
		, "UInt", 0		; hInstance
		, "UInt", 0)		; lpParam
	{
	msgbox, % A_ThisFunc "() failed with error " DllCall("GetLastError")
	Return 0
	}
sz := A_PtrSize=8 ? 16 : 12
VarSetCapacity(rbi, sz, 0)			; REBARINFO struct
NumPut(sz, rbi, 0, "UInt")
NumPut(hIL ? "1" : "0", rbi, 4, "UInt")	; only RBIM_IMAGELIST (0x1) is valid
NumPut(hIL, rbi, 8, Ptr)
if !DllCall("SendMessage" AW
		, Ptr, hwnd
		, "UInt", 0x404			; RB_SETBARINFO
		, "UInt", 0
		, Ptr, &rbi)
	{
	msgbox, % A_ThisFunc "() failed in RB_SETBARINFO."
	Return 0
	}
idx++
Return hwnd
}
;================================================================
; fStyle accepted values:
; RBBS_BREAK=0x1 RBBS_FIXEDSIZE=0x2 RBBS_CHILDEDGE=0x4 RBBS_HIDDEN=0x8 RBBS_NOVERT=0x10
; RBBS_FIXEDBMP=0x20 RBBS_VARIABLEHEIGHT=0x40 RBBS_GRIPPERALWAYS=0x80
; RBBS_NOGRIPPER=0x100 RBBS_USECHEVRON=0x200 RBBS_HIDETITLE=0x400 RBBS_TOPALIGN=0x800
; fMask accepted values:
; RBBIM_STYLE=0x1 RBBIM_COLORS=0x2 RBBIM_TEXT=0x4 RBBIM_IMAGE=0x8 RBBIM_CHILD=0x10
; RBBIM_CHILDSIZE=0x20 RBBIM_SIZE=0x40 RBBIM_BACKGROUND=0x80 RBBIM_ID=0x100
; RBBIM_IDEALSIZE=0x200 RBBIM_LPARAM=0x400 RBBIM_HEADERSIZE=0x800

RB_Add(hRB, hCW, txt="", sb="", pos="0", cx="", tbbh="", fStyle="0x4C1", hs="0")
;================================================================
{
Global Ptr, AW
Static idx=0
IEver=4					; assume IE version > 3
sz := 4*(14+(IEver>3)*6+(GetOSver() >= 6.0)*5)+16*(A_PtrSize=8)
VarSetCapacity(rbbi, sz, 0)	; REBARBANDINFO
; RBBIM_COLORS RBBIM_TEXT RBBIM_BACKGROUND RBBIM_STYLE RBBIM_CHILD RBBIM_CHILDSIZE RBBIM_SIZE RBBIM_ID RBBIM_IDEALSIZE
fMask := 0x1|0x2|0x4|0x10|0x200|0x40|(sb ? 0x80 : 0)|0x100|0x20|(hs ? 0x800: 0)
;fStyle := 0x1|0x40|0x80|0x400 ; RBBS_BREAK/VARIABLEHEIGHT/GRIPPERALWAYS/HIDETITLE
ControlGetPos,,, w, h,, ahk_id %hCW%
cx := cx ? cx : (w ? w+4 : "200")
if tbbh
	{
	StringSplit, h, tbbh, |
	Loop, 4
		h%A_Index% := (h%A_Index% ? h%A_Index% : h1 ? h1 : h ? h : "16")
	h4 *= 5
	}
else
	{
	h1 := h2 := h3 := (h ? h+2 : "21")
	h4 := 5*h1					; arbitrarily chosen max band height as 5*band height
	}
; check if sb is number and load its resource from current file
if sb is integer
	hbmBack := DllCall("LoadImage" AW
					, Ptr, -1
					, "UInt", sb
					, "UInt", 0
					, "UInt", cx
					, "UInt", h4
					, "UInt", 0x2000)
else Loop, %sb%
	{
	if A_LoopFileExt in bmp,ico
		hbmBack := DllCall("LoadImage" AW
					, Ptr, 0
					, "Str", sb
					, "UInt", 0
					, "UInt", cx
					, "UInt", h4
					, "UInt", 0x2010)	; LR_CREATEDIBSECTION LR_LOADFROMFILE
	else
		{
		if sb is not integer
			i := &sb
		else i := sb
		hbmBack := DllCall("LoadImage" AW
					, Ptr, -1
					, "UInt", i
					, "UInt", 0
					, "UInt", cx
					, "UInt", h4
					, "UInt", 0x2000)
		}
	}
; the structure is not x64 safe !!!
NumPut(sz, rbbi, 0, "UInt")			; cbSize
NumPut(fMask, rbbi, 4, "UInt")		; fMask
NumPut(fStyle, rbbi, 8, "UInt")		; fStyle
NumPut(&txt, rbbi, 20, "UInt")		; lpText
NumPut(hCW, rbbi, 32, Ptr)			; hwndChild
NumPut(cx, rbbi, 36, "UInt")			; cxMinChild
NumPut(h3, rbbi, 40, "UInt")			; cyMinChild
NumPut(cx, rbbi, 44, "UInt")			; cx
NumPut(hbmBack, rbbi, 48, Ptr)		; hbmBack
NumPut(idx, rbbi, 52, "UInt")			; wID
NumPut(h1, rbbi, 56, "UInt")			; cyChild
NumPut(h4, rbbi, 60, "UInt")			; cyMaxChild
NumPut(h2, rbbi, 64, "UInt")			; cyIntegral
NumPut(cx, rbbi, 68, "UInt")			; cxIdeal
NumPut(hs, rbbi, 76, "UInt")			; cxHeader
if !DllCall("SendMessage" AW
		, Ptr, hRB
		, "UInt", (A_IsUnicode ? 0x40A : 0x401)	; RB_INSERTBANDA/W
		, "UInt", pos-1
		, Ptr, &rbbi)
	{
	msgbox, % A_ThisFunc "() failed in RB_INSERTBAND" AW "."
	Return 0
	}
idx++
Return NumGet(rbbi, 52, "UInt")+1
}
;================================================================
RB_SizeBand(hRB, idx="1", d="1", i="0")
;================================================================
{
Global Ptr, AW
DllCall("SendMessage" AW
	, Ptr, hRB
	, "UInt", 0x41E + d				; RB_MAXIMIZEBAND/RB_MINIMIZEBAND
	, "UInt", idx-1
	, "UInt", (d ? i : "0"))
}
;================================================================
RB_Size(hRB, nw="", nh="")
;================================================================
{
Global Ptr, AW
;WinGetPos, x, y, w, h, % "ahk_id " DllCall("GetAncestor", "UInt", hRB, "UInt", 1)	; GA_PARENT
WinGetPos, x, y, w, h, % "ahk_id " DllCall("GetParent", Ptr, hRB)
VarSetCapacity(RC, 16, 0)
NumPut(0, RC, 0, "Int")
NumPut(0, RC, 4, "Int")
NumPut(nw ? nw : 0, RC, 8, "Int")		; NumPut(nw ? nw : w, RC, 8, "Int")
NumPut(nh ? nh : 0, RC, 12, "Int")		; NumPut(nh ? nh : h, RC, 12, "Int")
return DllCall("SendMessage" AW
			, Ptr, hRB
			, "UInt", 0x417		; RB_SIZETORECT
			, "UInt", 0
			, Ptr, &RC)
}
;================================================================
RB_Show(hRB, idx="1", s="1")
;================================================================
{
Global Ptr, AW
return DllCall("SendMessage" AW
			, Ptr, hRB
			, "UInt", 0x423		; RB_SHOWBAND
			, "UInt", idx-1
			, "UInt", (s ? "1" : "0"))
}
;================================================================
RB_Set(hRB, idx, p)	; not x64 safe !!!
;================================================================
{
; incompatible with previous versions !!! Added cxMinChild as first element in 'ic' string parameter
Global Ptr, AW
IEver=4
sz := 4*(14+(IEver>3)*6+(GetOSver() >= 6.0)*5)+16*(A_PtrSize=8)
VarSetCapacity(rbbi, sz, 0)	; REBARBANDINFO
NumPut(sz, rbbi, 0, "UInt")
fMask=0x271				; to be extended !!!
NumPut(fMask, rbbi, 4, "UInt")
if !DllCall("SendMessage" AW
		, Ptr, hRB
		, "UInt", (A_IsUnicode ? 0x41C : 0x41D)		; RB_GETBANDINFOA/W
		, "UInt", idx-1
		, Ptr, &rbbi)
	msgbox, % "Error in " A_ThisFunc "() : RB_GETBANDINFO" AW
NumPut(sz, rbbi, 0, "UInt")
fMask=0
Loop, Parse, p, %A_Space%, %A_Space%%A_Tab%
	{
	StringLeft, i, A_LoopField, 2
	StringTrimLeft, c, A_LoopField, 2
	if i=ic										; ic[height]|[integral]|[min]|[max]
		{
		fMask |= 0x220								; RBBIM_IDEALSIZE/CHILDSIZE
		StringSplit, h, c, |
		cyChild := ++h2
		cxMin := NumGet(rbbi, 36, "UInt")
;msgbox, h1=%h1% h2=%h2% h3=%h3% h4=%h4% h5=%h5%`ncxMin=%cxMin%
; cxMinChild yields large value if child toolbar is not properly set up
; Maybe add cxIdeal after cxMin in parameter string
		if (h1 != "")
			NumPut(h1, rbbi, 36, "UInt")				; cxMinChild
		NumPut(h4 ? h4 : cyChild, rbbi, 40, "UInt")		; cyMinChild
		NumPut(cyChild, rbbi, 56, "UInt")				; cyChild
		NumPut(h5 ? h5 : 5*cyChild, rbbi, 60, "UInt")	; cyMaxChild
		NumPut(h3 ? h3 : cyChild, rbbi, 64, "UInt")		; cyIntegral
		NumPut(NumGet(rbbi, 36, "UInt"), rbbi, 68, "UInt")	; cxIdeal
		}
	else if i=st
		{
		fMask |= 0x1								; RBBIM_STYLE
		NumPut(c, rbbi, 8, "UInt")					; fStyle
		}
	else if i=ch
		{
		fMask |= 0x10								;  RBBIM_CHILD
		NumPut(c, rbbi, 32, Ptr)						; hwndChild
		}
	else if i=cw
		{
		fMask |= 0x40								;  RBBIM_SIZE
		NumPut(c, rbbi, 44, "UInt")					; cx
		}
	else if i=bk
		{
		fMask |= 0x80								;  RBBIM_BACKGROUND
		NumPut(c, rbbi, 48, Ptr)						; hbmBack
		}
	}
NumPut(fMask, rbbi, 4, "UInt")
if !DllCall("SendMessage" AW
		, Ptr, hRB
		, "UInt", (A_IsUnicode ? 0x40B : 0x406)		; RB_SETBANDINFOA/W
		, "UInt", idx-1
		, Ptr, &rbbi)
	msgbox, % "Error in " A_ThisFunc "() : RB_SETBANDINFO" AW
if InStr(p, "bk")
	{
	RB_Show(hRB, idx-1, 0)
	RB_Show(hRB, idx-1, 1)
	}
}
;================================================================
RB_GetBand(hRB, idx, p)
;================================================================
{
Global Ptr, AW
if p=hr
	return DllCall("SendMessage" AW
				, Ptr, hRB
				, "UInt", 0x40E	; RB_GETROWHEIGHT
				, "UInt", idx-1
				, "UInt", 0)
IEver=4
sz := 4*(14+(IEver>3)*6+(GetOSver() >= 6.0)*5)+16*(A_PtrSize=8)
VarSetCapacity(rbbi, sz, 0)	; REBARBANDINFO
NumPut(sz, rbbi, 0, "UInt")
fMask=0x271				; RBBIM_IDEALSIZE/CHILDSIZE/CHILD/SIZE/STYLE to be extended !!!
NumPut(fMask, rbbi, 4, "UInt")
if !DllCall("SendMessage" AW
		, Ptr, hRB
		, "UInt", (A_IsUnicode ? 0x41C : 0x41D)		; RB_GETBANDINFOA/W
		, "UInt", idx-1
		, Ptr, &rbbi)
	msgbox, % "Error in " A_ThisFunc "() : RB_GETBANDINFO" AW
if p=w
	return NumGet(rbbi, 44, "UInt")	; cx
if p=wi
	return NumGet(rbbi, 68, "UInt")	; cxIdeal
if p=wm
	return NumGet(rbbi, 36, "UInt")	; cxMinChild
if p=hm
	return NumGet(rbbi, 40, "UInt")	; cyMinChild
if p=hx
	return NumGet(rbbi, 60, "UInt")	; cyMaxChild
if p=hi
	return NumGet(rbbi, 64, "UInt")	; cyIntegral
}
;================================================================
RB_Get(hRB, p)	; to be completed !!!
;================================================================
{
Global Ptr, AW
if p=h
	return DllCall("SendMessage" AW
				, Ptr, hRB
				, "UInt", 0x41B	; RB_GETBARHEIGHT
				, "UInt", 0
				, "UInt", 0)
if p=c
	return DllCall("SendMessage" AW
				, Ptr, hRB
				, "UInt", 0x40C	; RB_GETBANDCOUNT
				, "UInt", 0
				, "UInt", 0)
if p=r
	return DllCall("SendMessage" AW
				, Ptr, hRB
				, "UInt", 0x40D	; RB_GETROWCOUNT
				, "UInt", 0
				, "UInt", 0)
if InStr(p, "b")=1
	{
	StringTrimLeft, p, p, 1
	VarSetCapacity(rect, 16, 0)
	DllCall("SendMessage" AW
			, Ptr, hRB
			, "UInt", 0x422		; RB_GETBANDBORDERS
			, "UInt", p-1			; uBand
			, Ptr, &rect)
	Loop, 4
		r .= NumGet(rect, 4*(A_Index-1), "Int") " "
	return r
	}
if p=m
	{
	VarSetCapacity(rect, 16, 0)
	DllCall("SendMessage" AW
			, Ptr, hRB
			, "UInt", 0x428		; RB_GETBANDMARGINS (XP+)
			, "UInt", 0			; uBand
			, Ptr, &rect)
	Loop, 4
		r .= NumGet(rect, 4*(A_Index-1), "Int") " "
	return r
	}
}
;================================================================
RB_DelBand(hRB, idx=1)
;================================================================
{
Global Ptr, AW
return DllCall("SendMessage" AW
			, Ptr, hRB
			, "UInt", 0x402		; RB_DELETEBAND
			, "UInt", idx-1			; uBand
			, "UInt", 0)
}
;================================================================
RB_Cleanup(hRB, pos="")
;================================================================
{
if !pos
	{
	Loop
		if !RB_DelBmp(hRB, A_Index)
			break
	}
else RB_DelBmp(hRB, pos)
}
;================================================================
RB_Destroy(hRB)
;================================================================
{
Global
if DllCall("IsWindow", Ptr, hRB)
	return DllCall("DestroyWindow", Ptr, hRB)
}
;================================================================
RB_DelBmp(hRB, pos)
;================================================================
{
Global Ptr, AW
if !DllCall("IsWindow", Ptr, hRB)
	return FALSE
IEver=4					; assume IE version > 3
sz := 4*(14+(IEver>3)*6+(GetOsver() >= 6.0)*5)+16*(A_PtrSize=8)
VarSetCapacity(rbbi, sz, 0)	; REBARBANDINFO
if !DllCall("SendMessage" AW
		, Ptr, hRB
		, "UInt", (A_IsUnicode ? 0x41C : 0x41D)	; RB_GETBANDINFOA/W
		, "UInt", pos-1
		, Ptr, &rbbi)
	Return 0
if r := NumGet(rbbi, 48, Ptr)			; hbmBack
	DllCall("DeleteObject", Ptr, r)
return TRUE
}
;#include func_GetHwnd.ahk
#include *i func_GetOSver.ahk
