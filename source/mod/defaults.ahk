; Transparency: M=magenta A=alpha E=either M or A N=none F=fake 32bit. It comes as first char in defaults2+

; State group items index. It comes as second char in defaults2+
SIL1 := "Normal"
SIL2 := "Active,Inactive"
SIL3 := "Unchecked,Checked"
SIL4 := "Unchecked,Checked,Undetermined"
SIL5 := "Up,Down,Left,Right"

; Group state index. It comes as third char in defaults2+
GSL0 := "Normal"
GSL1 := "Normal/Hovered/Pressed"
GSL2 := "Normal/Hovered/Pressed/Disabled"
GSL3 := "Normal/Hovered/Pressed/Unused/Disabled"
GSL4 := "Normal/Hovered/Pressed/Disabled/Selected"
GSL5 := "Normal/hovered/Pressed/Disabled/Checked/Checked hovered"
GSL6 := "Normal/Hovered/Selected/Hung/Unused/Hovered selected/Flashing"

; Data types: S=string I=integer C=color ref B=bool. Ranges can be specified after default value.

defaults0 := ",I,I,I,I,S,S,I255|0 255,I255|0 255,B0,I|0 255,,,,,,,,"
defaults1 := "S,S,I18,I0,C0 0 0,C0 0 0,C0 0 0,I30,I0,I1,I0,I0,B1,C0 0 0,B0,C0 0 0,B0,I,I,I"
defaults2 := "F14S,I5"		; button
defaults3 := "N42S,I12"		; checkbox
defaults4 := "F32S,I8"		; radio button
defaults5 := "M10S,I"		; menu
defaults6 := "F22S,I8"		; close button
defaults7 := "F22S,I8"		; closeonly button
defaults8 := "F22S,I8"		; minimize button
defaults9 := "F22S,I8"		; restore button
defaults10 := "F22S,I8"		; maximize button
defaults11 := "F22S,I8"		; help button
defaults12 := "M20S,I2"		; caption
defaults13 := "N20S,I2"		; frame top-left
defaults14 := "N20S,I2"		; frame top-right
defaults15 := "N20S,I2"		; frame left
defaults16 := "N20S,I2"		; frame right
defaults17 := "N20S,I2"		; frame bottom
defaults18 := "F12S,I4"		; combobox
defaults19 := "F52S,I16"	; scroll arrows
defaults20 := "N12S,I4"		; scroll shaft vertical
defaults21 := "N12S,I4"		; scroll thumb horizontal
defaults22 := "N12S,I4"		; scroll shaft horizontal
defaults23 := "N12S,I4"		; scroll thumb vertical
defaults24 := "M10S,I1"		; groupbox
defaults25 := "N10S,I1"		; taskbar
defaults26 := "N10S,I1"		; taskbar vertical
defaults27 := "N10S,I1"		; taskbar tray
defaults28 := "M15S,I6"		; taskbar toolbar
defaults29 := "M16S,I7|6 7"	; taskbar button (count can be 6 if next item is specified)
defaults30 := "M10S,I1"		; taskbar button flash
defaults31 := "E11S,I3"		; start button (can have language variants)
defaults32 := "M15S,I6"		; IE toolbar
defaults33 := "F10S,I1"		; progress track
defaults34 := "N10S,I1"		; progress chunk
defaults35 := "M13S,I5"		; trackbar down
defaults36 := "M13S,I5"		; trackbar horizontal
defaults37 := "M13S,I5"		; trackbar up
defaults38 := "M13S,I5"		; trackbar left
defaults39 := "M13S,I5"		; trackbar vertical
defaults40 := "M13S,I5"		; trackbar right
