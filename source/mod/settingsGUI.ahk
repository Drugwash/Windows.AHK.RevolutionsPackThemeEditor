/*
#include ..\updates.ahk
nmclr := ntfclr := zfclr := ctrlcolor := 0xFFFFFF, menucolor := 0xE050E0
skinsz := 2, skinSrc := 0, pref_ttdelay := 700, pref_ttshow := 15000, pvsq := 10, pvclr1 := 0x0
pvclr2 := 0x202020, maxzoom := 50, maxzw := 500, bst := 0, zs := "", s2Clr := 0
hBrush := checkBrush(pvsq, pvclr1, (s2Clr ? pvclr2 : ""))

Gui, 1:Margin, 0, 0
Gui, 1:Add, Picture, x0 y0 w100 h80 0x10E hwndhPbk,
Gui, 1:Add, Text, x2 y2 BackgroundTrans vnotif, notif
Gui, 1:Add, Text, x2 y40 BackgroundTrans vwarnZoom, warn zoom
Gui, 1:Add, Text, x2 y60 BackgroundTrans vwarnRsz, warn rsz
Gui, 1:Add, Text, x2 y90 w200 h200 vdebug,
Gui, Show
OnExit, cleanup
*/
;================================================================
Sshow:
Szs := zs ? 1 : 0
Spvclr1 := pvclr1, Spvclr2 := pvclr2, Snmclr := nmclr, Sntfclr := ntfclr, Szfclr := zfclr, skinSrc1 := !skinSrc
SskinPath := skinPath ? skinPath : defskin
LVopt := "-E0x200 +Border -hdr +ReadOnly AltSubmit"
if !nWP4
	{
	nWP4 := RegisterCallback("WP4", "F", 4, "")
	hSTColor := 0xE0FFFF
	hSTBrush := DllCall("CreateSolidBrush", "UInt", hSTColor)
	}
Gui, 4:+Owner1
Gui, 1:+Disabled
Gui, 4:+OwnDialogs
Gui, 4:Color,  %menucolor%, %ctrlcolor%
Gui, 4:Font, s8, Tahoma
Gui, 4:+LastFound
hGui4 := WinExist()
oWP4 := DllCall("SetWindowLong", Ptr, hGui4, "Int", -4, "Int", nWP4, Ptr)
Gui, 4:Add, GroupBox, x3 y9 w458 h92 +Theme +0x8007 BackgroundTrans, 
Gui, 4:Add, Text, x12 y4 h17 Border hwndhST1, %A_Space%Appearance%A_Space%

Gui, 4:Add, GroupBox, x6 y28 w240 h70 +Theme +0x8007 BackgroundTrans, 
Gui, 4:Add, Text, x12 y23 h17 Border hwndhST2, %A_Space%Skinning%A_Space%
Gui, 4:Add, Radio, x10 y42 w178 h16 Checked vSskinInt gSradSkin, Internal image strip
Gui, 4:Add, Radio, x10 y58 w18 h18 Checked%skinSrc% hwndhSetRad vSskinSrc gSradSkin, 
Gui, 4:Add, Edit, x28 y58 w197 h18 E0x200 Right hwndhSetEdt vSskinPath gSradSkin, %SskinPath%
Gui, 4:Add, Button, x227 y58 w15 h18 vSskinBws gSskinBws, �
Gui, 4:Add, Text, x10 y76 w175 h18 +0x200 +Right, Width of each color bar in the strip:
Gui, 4:Add, Edit, x185 y76 w40 h18 -Multi +Right Disabled%skinSrc1% vSskinsz1, %skinsz%
Gui, 4:Add, UpDown, x185 y76 w13 h18 Left Range2-32 Disabled%skinSrc1% vSskinsz, %skinsz%
Gui, 4:Add, Text, x227 y76 w15 h18 +0x200, px.

Gui, 4:Add, GroupBox, x250 y28 w208 h70 +Theme +0x8007 BackgroundTrans, 
Gui, 4:Add, Text, x256 y23 h17 Border hwndhST3, %A_Space%Tooltips%A_Space%
Gui, 4:Add, Checkbox, x254 y42 w201 h16 Disabled Checked%bst% vSbst, Balloon style (needs ME files on 98SE)
Gui, 4:Add, Text, x254 y58 w140 h18 +Right +0x200, Delay before showing:
Gui, 4:Add, Edit, x394 y58 w40 h18 -Multi +Right vSpref_ttdelay, %pref_ttdelay%
Gui, 4:Add, Text, x436 y58 w15 h18 +0x200, ms.
Gui, 4:Add, Text, x254 y76 w140 h18 +Right +0x200, Time to show:
Gui, 4:Add, Edit, x394 y76 w40 h18 -Multi +Right vSpref_ttshow, %pref_ttshow%
Gui, 4:Add, Text, x436 y76 w15 h18 +0x200, ms.

Gui, 4:Add, GroupBox, x3 y111 w458 h171 +Theme +0x8007 BackgroundTrans, 
Gui, 4:Add, Text, x12 y106 h17 Border hwndhST4, %A_Space%Preview window%A_Space%

Gui, 4:Add, GroupBox, x6 y130 w128 h72 +Theme +0x8007 BackgroundTrans, 
Gui, 4:Add, Text, x12 y125 h17 Border hwndhST5, %A_Space%Background%A_Space%
Gui, 4:Add, Text, x10 y144 w65 h18 +0x200 +Right, Square size:
Gui, 4:Add, Edit, x75 y144 w40 h18 -Multi +Right vSpvsq1 gSupdPv, %pvsq%
Gui, 4:Add, UpDown, x75 y144 w13 h18 Left Range1-30 vSpvsq gSupdPv, %pvsq%
Gui, 4:Add, Text, x117 y144 w15 h18 +0x200, px.
Gui, 4:Add, Text, x10 y162 w65 h18 +0x200 +Right, First color:
Gui, 4:Add, ListView, x75 y162 w40 h18 %LVopt% Background%pvclr1% hwndhChk1 vSpvclr1 gSetSelClr, 
Gui, 4:Add, Text, x10 y180 w65 h18 +0x200 +Right, Second color:
Gui, 4:Add, ListView, x75 y180 w40 h18 %LVopt% Background%pvclr2% hwndhChk2 vSpvclr2 gSetSelClr, 
Gui, 4:Add, Checkbox, x115 y180 w16 h18 +Right Checked%s2Clr% vSs2Clr gSupdPv, 

Gui, 4:Add, GroupBox, x138 y130 w108 h72 +Theme +0x8007 BackgroundTrans, 
Gui, 4:Add, Text, x144 y125 h17 Border hwndhST6, %A_Space%Notifications%A_Space%
Gui, 4:Add, Text, x142 y144 w65 h18 +0x200 +Right, Generic:
Gui, 4:Add, Text, x142 y162 w65 h18 +0x200 +Right, Zoom:
Gui, 4:Add, Text, x142 y180 w65 h18 +0x200 +Right, Zoom factor:
Gui, 4:Add, ListView, x207 y144 w35 h18 %LVopt% Background%nmclr% hwndhLVntf1 vSnmclr gSetSelClr, 
Gui, 4:Add, ListView, x207 y162 w35 h18 %LVopt% Background%ntfclr% hwndhLVntf2 vSntfclr gSetSelClr, 
Gui, 4:Add, ListView, x207 y180 w35 h18 %LVopt% Background%zfclr% hwndhLVntf3 vSzfclr gSetSelClr, 

Gui, 4:Add, GroupBox, x6 y209 w240 h70 +Theme +0x8007 BackgroundTrans, 
Gui, 4:Add, Text, x12 y204 h17 Border hwndhST7, %A_Space%Zoom%A_Space%
Gui, 4:Add, Checkbox, x10 y223 w233 h16 Disabled%w9x% Checked%Szs% vSzs, Show zoom window shadow (XP+)
Gui, 4:Add, Text, x10 y239 w170 h18 +0x200 +Right, Maximum zoom factor:
Gui, 4:Add, Edit, x180 y239 w45 h18 -Multi +Right vSmaxzoom1, %maxzoom%
Gui, 4:Add, UpDown, x180 y239 w13 h18 Left Range1-99 vSmaxzoom, %maxzoom%
Gui, 4:Add, Text, x228 y239 w15 h18 +0x200, X
Gui, 4:Add, Text, x10 y257 w170 h18 +0x200 +Right, Maximum magnifier window size:
Gui, 4:Add, Edit, x180 y257 w45 h18 -Multi +Right vSmaxzw1, %maxzw%
Gui, 4:Add, UpDown, x180 y257 w13 h18 Left Range25-999 vSmaxzw, %maxzw%
Gui, 4:Add, Text, x228 y257 w15 h18 +0x200, px.
Gui, 4:Add, Picture, x250 y136 w208 h143 +0x10E Border hwndhSPbk vSPbk, 
Gui, 4:Font, bold c%nmclr%,
Gui, 4:Add, Text, x254 y260 w120 h16 +0x200 BackgroundTrans hwndhSPvwTxt1 vSPvwTxt1, Generic notification
Gui, 4:Font, bold c%ntfclr%,
Gui, 4:Add, Text, x254 y140 w120 h16 +0x200 BackgroundTrans hwndhSPvwTxt2 vSPvwTxt2, Zoom changed
Gui, 4:Font, bold c%zfclr%,
Gui, 4:Add, Text, x254 y240 w120 h16 +0x200 BackgroundTrans hwndhSPvwTxt3 vSPvwTxt3, Zoom %zf%X
Gui, 4:Font, Norm,

Gui, 4:Add, Button, x10 y286 w100 h30, OK
Gui, 4:Add, Button, x117 y286 w100 h30, Cancel
; Generated using SmartGuiXP Creator mod 4.3.29.7
Gui, 4:Submit, NoHide
setTitles=%hST1%,%hST2%,%hST3%,%hST4%,%hST5%,%hST6%,%hST7%
DllCall("DeleteObject", Ptr, hSBmPbk)
hSBmPbk := setBrush(hSPbk, hBrush)
WinSet, Redraw,, ahk_id %hSPbk%
WinSet, Redraw,, ahk_id %hSPvwTxt1%
WinSet, Redraw,, ahk_id %hSPvwTxt2%
WinSet, Redraw,, ahk_id %hSPvwTxt3%
Gui, 4:Show, Center w464 h319, RPTE - Settings
Return
;================================================================
4ButtonOK:
;================================================================
Gui, 4:Submit, NoHide
SskinPath := SskinPath=defskin ? "" : SskinPath
if (skinSrc != SskinSrc OR skinPath != SskinPath OR skinsz != Sskinsz)
	{
	IL_Destroy(skin)
	skin := (SskinSrc && SskinPath) ? SskinPath : defskin
	GetBmpInfo(skin "|" skinres, sbw, sbh, sbpp)
	if Sskinsz not between 2 and %sbw%		; Reset skin band width to minimum
		Sskinsz=2						; if exceeds total strip width
	if (cSkin > sbw//Sskinsz)					; Reset current skin index to 1
		cSkin=1							; if exceeds total band count in strip
	DllCall("DeleteObject", Ptr, hSkin)
	hSkin := GetBmp(cSkin, Sskinsz, skin, skinres, 0)	; handle to skin bitmap (returns ImageList handle in 'skin')
	Loop, %skincount%
		DllCall("DeleteObject", Ptr, hDSkin%A_Index%)
	skincount := ILC_Count(skin)				; number of skin images
	Gui, 3:Destroy
	gosub buildSkinSel
	gosub ChSkin							; If user cancels skin selection, he's gonna be up for a surprize!
	}
; update all variables to new values
skinsz := Sskinsz, skinSrc := SskinSrc, pref_ttdelay := Spref_ttdelay
pref_ttshow := Spref_ttshow, pvsq := Spvsq, pvclr1 := Spvclr1, pvclr2 := Spvclr2, s2Clr := Ss2Clr
nmclr := Snmclr, ntfclr := Sntfclr, zfclr := Szfclr, maxzoom := Smaxzoom, maxzw := Smaxzw, bst := Sbst
zs := Szs ? "s" : ""
zf := zf>maxzoom ? maxzoom : zf
zw := zw>maxzw ? maxzw : zw
DllCall("DeleteObject", Ptr, hBrush)
DllCall("DeleteObject", Ptr, hBmPbk)
hBrush := checkBrush(pvsq, pvclr1, (s2Clr ? pvclr2 : ""))
hBmPbk := setBrush(hPbk, hBrush)
Sleep(1)
WinSet, Redraw,, ahk_id %hPbk%
WinSet, Redraw,, ahk_id %hPv1%
Gui, 1:Font, c%ntfclr% Bold
GuiControl, 1:Font, notif
Gui, 1:Font, c%zfclr% Bold
GuiControl, 1:Font, warnZoom
Gui, 1:Font, c%nmclr% Bold
GuiControl, 1:Font, warnRsz
Gui, 1:Font, cDefault Norm
gosub tabchange
;================================================================
4GuiClose:
4ButtonCancel:
DllCall("SetWindowLong", Ptr, hGui4, "Int", -4, "Int", oWP4, Ptr)
Gui, 1:-Disabled
Gui, 4:Destroy
/*
GuiControl, 1:, debug, nmclr=%nmclr% ntfclr=%ntfclr% zfclr=%zfclr% skinsz=%skinsz% skinSrc=%skinSrc% skinPath=%skinPath% pref_ttdelay=%pref_ttdelay% pref_ttshow=%pref_ttshow% pvsq=%pvsq% pvclr1=%pvclr1% pvclr2=%pvclr2% maxzoom=%maxzoom% maxzw=%maxzw% bst=%bst% zs=%zs%
*/
return
;================================================================
SradSkin:
;================================================================
Gui, 4:Submit, NoHide
if A_GuiControl in SskinPath,SskinSrc
	{
	GuiControl, 4:, SskinSrc, 1
	GetBmpInfo(SskinPath "|" skinres, sbw, sbh, sbpp)
	if (Sskinsz1 > Sskinsz)
		GuiControl, 4:, Sskinsz1, %Sskinsz%
	GuiControl, 4:+Range2-%sbw%, Sskinsz
	GuiControl, 4:Enable, Sskinsz
	GuiControl, 4:Enable, Sskinsz1
	}
else
	{
	GuiControl, 4:, SskinSrc, 0
	GetBmpInfo(defskin, sbw, sbh, sbpp)
	GuiControl, 4:, Sskinsz1, 2
	GuiControl, 4:+Range2-%sbw%, Sskinsz
	GuiControl, 4:Disable, Sskinsz
	GuiControl, 4:Disable, Sskinsz1
	}
return
;================================================================
SskinBws:
;================================================================
FileSelectFile, i, 3, %SskinPath%, Select new bitmap for skin strip:, Bitmaps (*.bmp)
if (!i or ErrorLevel)
	return
SskinPath := i
GuiControl, 4:, SskinPath, %SskinPath%
GuiControl, 4:, skinSrc, 1
GetBmpInfo(skin "|" skinres, sbw, sbh, sbpp)
GuiControl, 4:+Range2-%sbw%, Sskinsz
if (Sskinsz1 > sbw)
	GuiControl, 4:, Sskinsz1, %sbw%
GuiControl, 4:Enable, Sskinsz
GuiControl, 4:Enable, Sskinsz1
Gui, 4:Submit, NoHide
return
;================================================================
SupdPv:
;================================================================
Gui, 4:Submit, NoHide
btnclr := A_GuiControl
goto SsetClr
return
;================================================================
SetSelClr:
;================================================================
Gui, 4:Submit, NoHide
if (A_GuiEvent != "Normal")
	return
btnclr := A_GuiControl
i := ChgColor(SwColor(%btnclr%), "BR", 4)
if ErrorLevel
	return
%btnclr% := i
GuiControl, 4:+Background%i%, %btnclr%
;================================================================
SsetClr:
if btnclr in Spvclr1,Spvclr2,Ss2Clr,Spvsq,Spvsq1
	{
	DllCall("DeleteObject", Ptr, hSBrush)	; delete these on exit
	DllCall("DeleteObject", Ptr, hSBmPbk)
	hSBrush := checkBrush(Spvsq, Spvclr1, (Ss2Clr ? Spvclr2 : ""))
	hSBmPbk := setBrush(hSPbk, hSBrush)
	WinSet, Redraw,, ahk_id %hSPbk%
	WinSet, Redraw,, ahk_id %hSPvwTxt1%
	WinSet, Redraw,, ahk_id %hSPvwTxt2%
	WinSet, Redraw,, ahk_id %hSPvwTxt3%
	}
else if btnclr=Snmclr
	{
	Gui, 4:Font, c%i% bold
	GuiControl, 4:Font, SPvwTxt1
	GuiControl, 4:, SPvwTxt1, Generic notification
	Gui, 4:Font, cDefault
	}
else if btnclr=Sntfclr
	{
	Gui, 4:Font, c%i% bold
	GuiControl, 4:Font, SPvwTxt2
	GuiControl, 4:, SPvwTxt2, Zoom option change
	Gui, 4:Font, cDefault
	}
else if btnclr=Szfclr
	{
	Gui, 4:Font, c%i% bold
	GuiControl, 4:Font, SPvwTxt3
	GuiControl, 4:, SPvwTxt3, Zoom factor %zf%X
	Gui, 4:Font, cDefault
	}
return
;================================================================
WP4(hwnd, msg, wP, lP)
{
Global
SetFormat, Integer, H
lP+=0
SetFormat, Integer, D
if lP in %setTitles%
	if (msg=0x138)	; WM_CTLCOLORSTATIC
		{
;		DllCall("SetBkMode", Ptr, wP, "UInt", 0)	; OPAQUE
		DllCall("SetBkColor", Ptr, wP, "UInt", hSTColor)
		return hSTBrush
		}
return DllCall("CallWindowProc" AW, Ptr, oWP4, Ptr, hwnd, "UInt", msg, Ptr, wP, Ptr, lP)
}
/*
cleanup:
DllCall("DeleteObject", Ptr, hBrush)	; delete these on exit
DllCall("DeleteObject", Ptr, hSBrush)	; delete these on exit
DllCall("DeleteObject", Ptr, hSBmPbk)
ExitApp

#include ..\func\func_Brush.ahk
#include ..\func\func_ChgColor.ahk
#include ..\func\func_GetHwnd.ahk
*/
