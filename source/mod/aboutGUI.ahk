;================================================================
initAbout:
;================================================================
hDCs := DllCall("GetDC", Ptr, 0, Ptr)
hDC := DllCall("CreateCompatibleDC", Ptr, hDCs, Ptr)
hBB := DllCall("CreateCompatibleBitmap", Ptr, hDCs, "Int", 400, "Int", 300, Ptr)
hBmo := DllCall("SelectObject", Ptr, hDC, Ptr, hBB, Ptr)
DllCall("ReleaseDC", Ptr, 0, Ptr, hDCs)
DllCall("SetBkMode", Ptr, hDC, "UInt", 0)	; OPAQUE
DllCall("PatBlt"
	, Ptr, hDC
	, "Int", 0
	, "Int", 0
	, "UInt", 400
	, "UInt", 300
	, "UInt", 0xF00062)	; WHITENESS
w := h := 80
if !A_IsCompiled
	{
	bmpPath = res\photo.bmp
	hBmp := DllCall("LoadImage" AW
				, Ptr, 0
				, "Str", bmpPath
				, "UInt", 0
				, "Int", 0
				, "Int", 0
				, "UInt", 0x2010
				, Ptr)
	hDC2 := DllCall("CreateCompatibleDC", Ptr, hDC, Ptr)
	hBm2o := DllCall("SelectObject", Ptr, hDC2, Ptr, hBmp, Ptr)
	DllCall("BitBlt"
		, Ptr, hDC
		, "Int", 10
		, "Int", 10
		, "Int", w
		, "Int", h
		, Ptr, hDC2
		, "Int", 0
		, "Int", 0
		, "UInt", 0x00CC0020)	; SRCCOPY
	DllCall("DeleteObject", Ptr, DllCall("SelectObject", Ptr, hDC2, Ptr, hBm2o, Ptr))
	DllCall("DeleteDC", Ptr, hDC2)
	DllCall("DeleteObject", Ptr, hBmp)
	}
else
	{
	bpp := 24
	hResInfo := DllCall("FindResource" AW, Ptr, 0, "Str", "#400", "UInt", 10, Ptr)	; RT_RCDATA
	hResData := DllCall("LoadResource", Ptr, 0, Ptr, hResInfo, Ptr)
	hResPtr := DllCall("LockResource", Ptr, hResData, Ptr)
;	szres := DllCall("SizeofResource", Ptr, 0, Ptr, hResInfo, "UInt")
	VarSetCapacity(BMI, 44, 0)	; BITMAPINFO struct
	NumPut(40, BMI, 0, "UInt")
	NumPut(w, BMI, 4, "UInt")
	NumPut(h, BMI, 8, "UInt")
	NumPut(1, BMI, 12, "UShort")
	NumPut(bpp, BMI, 14, "UShort")
	
	r := DllCall("StretchDIBits"
		, Ptr, hDC
		, "Int", 10
		, "Int", 10
		, "Int", w
		, "Int", h
		, "Int", 0
		, "Int", 0
		, "Int", w
		, "Int", h
		, Ptr, hResPtr
		, Ptr, &BMI
		, "UInt", 0			; iUsage DIB_RGB_COLORS
		, "UInt", 0x00CC0020)	; dwRop SRCCOPY
	
	DllCall("FreeResource", Ptr, hResData)
	VarSetCapacity(BMI, 0)		; free memory
	}
hAbtBrush := DllCall("CreatePatternBrush", Ptr, hBB,Ptr)
DllCall("DeleteObject", Ptr, DllCall("SelectObject", Ptr, hDC, Ptr, hBmo, Ptr))
DllCall("DeleteDC", Ptr, hDC)
return
;================================================================
about2:
;================================================================
if !nWPa
	nWPa := RegisterCallback("WPa", "F", 4, "")
swabt=1
aboutcopy =
(LTrim
%comment%
by %author%
version %version% %releaseT%
released %releaseD%
contact: %authormail%

This application is open-source,
created in AutoHotkey 1.0.48.05
under Windows 98SE.
Running under AHK %A_AhkVersion% %AW%.
Source code also compatible with AHK_L.

Credit goes to Tihiy, author of the
Revolutions Pack, also for descriptions.
Original docs: http://tihiy.net/uber/
Main pack: http://tihiy.net/files/RP9.exe
Extra: http://tihiy.net/files/RP9Updates.exe

Icons kindly provided by Mark James at:
http://www.famfamfam.com/lab/icons/silk/
Thank you CharlesF at MSFN for testing.

EVERYTHING should be free !
)
high = 270
Gui, 2:-Caption +ToolWindow +Border +Owner1
Gui, 2:+LastFound
hAbout := WinExist()
oWPa := DllCall("SetWindowLong", Ptr, hAbout, "Int", -4, Ptr, nWPa, Ptr)
Gui, 2:Add, GroupBox, x10 y-14 w380 h320 AltSubmit,
Gui, 2:Font, S7 bold, Verdana
Gui, 2:Add, Button, x391 y272 w8 h25 vabtS gabtsw, .
abtS_TT := "Toggle between scrolling mode and static mode"
Gui, 2:Add, Text, x90 y11 w226 Center Hidden vabt1, %aboutcopy%
abt1_TT :=
Gui, 2:Add, Edit, x11 y3 w378 h268 -E0x200 -0x200000 Center Hidden vabted, %aboutcopy%
Gui, 2:Add, Button, x160 y272 w80 h25 vabtC gabtclose, &Close
abtC_TT :=
Gui, 2:Show, w400 h300, About %appname%
GuiControl, 2:Show, abt1
scroll:
Loop, % high *2
	{
	vert := high - A_Index
	GuiControl, 2:Move, abt1, % "y" vert "h" (high - vert)
	sleep, % (A_Index=high OR A_Index=high*2-30 ? 4500 : 50)
	if !swabt
		break
	}
swabt=1
;================================================================
abtsw:
;================================================================
swabt := !swabt
if !swabt
	goto noscroll
GuiControl, 2:Show, abt1
GuiControl, 2:Hide, abted
SetTimer, scroll, -1
return
;================================================================
noscroll:
;================================================================
GuiControl, 2:Hide, abt1
GuiControl, 2:Show, abted
return
;================================================================
abtclose:
;================================================================
Gui, 2:Font, norm
Gui, 2:Destroy
return
;================================================================
WPa(hwnd, msg, wP, lP)
{
Global
if (msg = 0x138)	; WM_CTLCOLORSTATIC
	{
	DllCall("SetTextColor", Ptr, wP, "UInt", 0x0000FF)
	DllCall("SetBkMode", Ptr, wP, "UInt", 0)	; TRANSPARENT
	return DllCall("GetStockObject", "UInt", 5, Ptr)  ; HOLLOW_BRUSH
	}
else if (msg = 0x133)	; WM_CTLCOLOREDIT
	{
	DllCall("SetTextColor", Ptr, wP, "UInt", 0x500050)
;	DllCall("SetBkColor", Ptr, wP, "UInt", 0xFFFFFF)
	DllCall("SetBkMode", Ptr, wP, "UInt", 1)	; TRANSPARENT
	return hAbtBrush
;	return DllCall("GetStockObject", "UInt", 5)  ; WHITE_BRUSH (5=HOLLOW_BRUSH)
	}
else if (msg=0x136)	; WM_CTLCOLORDLG
	{
	DllCall("SetBkColor", Ptr, wP, "UInt", 0xFFFFFF)
	DllCall("SetBkMode", Ptr, wP, "UInt", 0)		; TRANSPARENT
	return hAbtBrush
	}
return DllCall("CallWindowProc" AW, Ptr, oWPa, Ptr, hwnd, "UInt", msg, Ptr, wP, Ptr, lP)
}
