; Revolutions Pack Theme Editor
; � Drugwash, April-May 2014
;================================================================
;	DIRECTIVES
;================================================================
#NoEnv
#SingleInstance, force
SetBatchLines, -1
SetControlDelay, -1
SetWinDelay, -1
ListLines, Off
SetFormat, Integer, D
DetectHiddenWindows, On
CoordMode, Mouse, Relative
;================================================================
;	IDENTIFICATION
;================================================================
appname=Revolutions Pack Theme Editor
apphome=http://my.cloudme.com/#drugwash/mytools/RPTE
comment=Easily create/check/fix RP9 themes
author=Drugwash
authormail=drugwash@aol.com
version=1.0.0.15
releaseD=April 15, 2018
releaseT := "private alpha " (A_IsUnicode ? "Unicode" : "ANSI")
debug=1
;================================================================
;	VARIABLES
;================================================================
;#include updates.ahk
updates()
DllGetVersion("shell32.dll", v, t, i, j)
if A_IsUnicode && (v<5.0 OR t="Win32")	; do this only when detecting Unicode version running on 9x
	{
	MsgBox, 0x1144, %appname%,
		(LTrim
		The previous warning regarding keyboard/mouse hook
		is built into the interpretor and cannot be disabled;
		bear in mind that without the hook, no hotkey will work.

		It happens because you're running the Unicode version
		on a Win9x system, which is not the intended usage.
		The ANSI version for Win9x machines is available at
		%apphome%

		Please be aware that this application will not ask to save
		changes on exit, in this configuration (to avoid a crash).

		Do you want to continue running this application now?
		)
	IfMsgBox No
		ExitApp
	abnormaluse=1
	}
appini := A_ScriptDir "\RPTE.ini"
inifile := A_ScriptDir "\no skin.ini"
dbgfile := A_ScriptDir "\debug.txt"
tfpath := A_WinDir "\Resources\Themes"	; get path to system's Themes folder
sections := "Global,Button,CheckBox,RadioButton,Menu,CloseButton,CloseButtonOnly,MinimizeButton
	,RestoreButton,MaximizeButton,HelpButton,CaptionN,FrameTopLeft,FrameTopRight,FrameLeft
	,FrameRight,FrameBottom,ComboBox,ScrollArrows,ScrollShaftV,ScrollThumbH,ScrollShaftH
	,ScrollThumbV,GroupBox,TaskBar,TaskBarVert,TaskBarTray,TaskbarToolbar,TaskBarButton
	,TaskBarButtonFlash,StartButton,IEToolbar,ProgressTrack,ProgressChunk,TrackBarD,TrackBarH
	,TrackBarU,TrackBarL,TrackBarV,TrackBarR"
inilist1 := "SkinName,Author,GlyphLimit,NoPremultAlpha,GroupBoxColor,TaskText,ClockText,TaskBarSize
	,TransCorn,ProgressShift,QLCutOffTop,QLCutOffBottom,ActiveShadow,ActiveShadowColor
	,InActiveShadow,InActiveShadowColor,AeroButtons,AeroButtonsMinRight,AeroButtonsTop
	,AeroButtonsTopMaxi"
inilist2 := "Bitmap,Count,MarginLeft,MarginRight,MarginTop,MarginBottom,GlyphBitmap,GlyphBitmapSmall
	,AlphaLevel,AlphaLevelGlyph,Tiled,NoPremultAlpha,,,,,,,,"
#include res\descriptions.txt
#include mod\defaults.ahk
TBB=23|New theme||0,12|Load theme||0,11|Load theme folder,1||0x1,6|Compression type||0
	,7|Save theme,8|Save theme as...,1||0x1,13|Set theme fonts||0,16|Add image||0
	,17|Remove image||0,18|Edit image||0,1||0x1,26|Add sound scheme||0,27|Add sound||0
	,28|Remove sound||0,1||0x1,22|Zoom||,21|Preview theme|0x12|0,3|Apply theme||0,1||0x1
	,32|Settings,1|About,10|Exit
if A_IsCompiled
	{
	FileCreateDir, %A_Temp%\RPTE
	FileInstall, res\RPTEicons16.dll , %A_Temp%\RPTE\RPTEicons16.dll
	FileInstall, res\RPTEicons24.dll , %A_Temp%\RPTE\RPTEicons24.dll
	FileInstall, res\RPTEicons32.dll , %A_Temp%\RPTE\RPTEicons32.dll
	}
;================================================================
;	PREFERENCES
;================================================================
iconlocal=%A_ScriptDir%\res\RPTE.ico
skinres=100							; skin resource number in exe
icons1 := (A_IsCompiled ? A_Temp "\RPTE" : "res") "\RPTEicons16.dll"
icons2 := (A_IsCompiled ? A_Temp "\RPTE" : "res") "\RPTEicons24.dll"
icons3 := (A_IsCompiled ? A_Temp "\RPTE" : "res") "\RPTEicons32.dll"
IniRead, ctrlcolor, %appini%, Preferences, CtrlColor, 0xFFFFFF
if ctrlcolor is not number
	ctrlcolor=0xFFFFFF					; let controls have a white background by default
IniRead, pref_isize, %appini%, Preferences, IconSize, s
if pref_isize not in s,m,l
	pref_isize=s						; set toolbar icon size to small by default
IniRead, bst, %appini%, Preferences, TooltipBalloon, 0
IniRead, pref_ttdelay, %appini%, Preferences, TooltipDelay, 700
IniRead, pref_ttshow, %appini%, Preferences, TooltipShow, 15000
IniRead, mnuvis, %appini%, Preferences, ShowMenu, 1
IniRead, tbvis, %appini%, Preferences, ShowToolbar, 1
IniRead, zf, %appini%, Zoom, ZoomFactor, 5
IniRead, maxzoom, %appini%, Zoom, ZoomMax, 50
IniRead, zr, %appini%, Zoom, ZoomRound, R
IniRead, zi, %appini%, Zoom, ZoomInvert, %A_Space%
IniRead, zs, %appini%, Zoom, ZoomShadow, S
IniRead, zw, %appini%, Zoom, ZoomWindow, 200
IniRead, maxzw, %appini%, Zoom, ZoomWindowMax, 500
IniRead, showframe, %appini%, PreviewItem, ShowFrame, 1
IniRead, cntall, %appini%, PreviewItem, ShowAllImgs, 1
IniRead, forceT, %appini%, PreviewItem, ForceTrans, 0
IniRead, forceTC, %appini%, PreviewItem, ForceTransColor, 0xFFFFFF
IniRead, aa, %appini%, PreviewItem, GlobalAlpha, 0
IniRead, ppa, %appini%, PreviewItem, PerPixelAlpha, 1
IniRead, pvsq, %appini%, PreviewItem, CheckeredSquare, 10
IniRead, s2Clr, %appini%, PreviewItem, UseTwoColors, 1
IniRead, pvclr1, %appini%, PreviewItem, CheckeredColor1, 0x202020
IniRead, pvclr2, %appini%, PreviewItem, CheckeredColor2, 0x000000
IniRead, ntfclr, %appini%, PreviewItem, ZoomNotifColor, 0xFFFFFF
IniRead, zfclr, %appini%, PreviewItem, ZoomFactorColor, 0xFFFFFF
IniRead, nmclr, %appini%, PreviewItem, NotificationColor, 0xFFFFFF
IniRead, skinSrc, %appini%, Preferences, ExternalSkin, 0
IniRead, skinPath, %appini%, Preferences, ExternalSkinPath, %A_Space%
IniRead, skinsz, %appini%, Preferences, SkinW, 2
IniRead, tSkin, %appini%, Preferences, SkinType, 0
if tSkin
	IniRead, cSkin, %appini%, Preferences, SkinColor, 23
else Random, cSkin, 1, 32				; use random skin color as default or by choice
;================================================================
;	INITIALIZATIONS
;================================================================
defskin := A_IsCompiled ? A_ScriptFullPath : "res\skin.bmp"
skin := skinSrc && skinPath ? skinPath : defskin
GetBmpInfo(skin "|" skinres, sbw, sbh, sbpp)	; Find bitmap width beforehand for calculations
if skinsz not between 2 and %sbw%		; Reset skin band width to minimum
	skinsz=2							; if exceeds total strip width
if (cSkin > sbw//skinsz)					; Reset current skin index to 1
	cSkin=1							; if exceeds total band count in strip
hSkin := GetBmp(cSkin, skinsz, skin, skinres, 0)	; handle to skin bitmap (returns ImageList handle in 'skin')
skincount := ILC_Count(skin)				; number of skin images
skincolor := GetPixelColor(hSkin, 0, 1)
menucolor := SwColor(skincolor)			; menu background color
opaq := 255-aa						; opacity level
zType := zi ? "Inverted zoom" : "Zoom"		; zoom message
hILs := ILC_Create(6, 1, "12x12", "M32")	; ILC_COLOR32 ILC_MASK
hILts := ILC_Create(42, 1, "16x16", "M32")	; ILC_COLOR32 ILC_MASK
hILtm := ILC_Create(42, 1, "24x24", "M32")	; ILC_COLOR32 ILC_MASK
hILtl := ILC_Create(42, 1, "32x32", "M32")	; ILC_COLOR32 ILC_MASK
hILte := ILC_Create(42, 1, "64x64", "M32")	; ILC_COLOR32 ILC_MASK
Loop, 5
	IL_Add(hILs, icons, 37+A_Index)
Loop, 38
	{
	IL_Add(hILts, icons1, A_Index)
	IL_Add(hILtm, icons2, A_Index)
	IL_Add(hILtl, icons3, A_Index)
	}
hIselY := ILC_GetIcon(hILts, 29)
hIselN := ILC_GetIcon(hILts, 35)
hIdft := ILC_GetIcon(hILts, 36)
hIori := ILC_GetIcon(hILts, 37)
UDLI := DllCall("GetUserDefaultLangID", "UShort")
SDLI := DllCall("GetSystemDefaultLangID", "UShort")
LangUser := FindLang(UDLI, "N")
LangSys := FindLang(SDLI, "N")
if debug
	FileAppend,
	(LTrim
	====== %A_Now% ======
	user lang: %LangUser%
	system lang: %LangSys%
	hIselY=%hIselY%
	hIselN=%hIselN%
	hIdft=%hIdft%
	hIori=%hIori%`n
	), %dbgfile%
;================================================================
;	TRAY  MENU
;================================================================
Menu, Tray, UseErrorLevel
if !A_IsCompiled
	Menu, Tray, Icon, %iconlocal%
Menu, Tray, Tip, %appname%
Menu, Tray, NoStandard
Menu, Tray, Add, Always on top, aot
Menu, Tray, Default, Always on top
Menu, Tray, Add, % (mnuvis ? "Hide" : "Show") " main menu", toggleMnu
Menu, Tray, Add, % (tbvis ? "Hide" : "Show") " toolbar", toggleTB
Menu, Tray, Add
Menu, Tray, Add, Options, options
Menu, Tray, Add, About, about
Menu, Tray, Add
;if debug
	Menu, Tray, Add, Reload, reload
Menu, Tray, Add, Exit, saveandexit
Menu, Tray, Color, %menucolor%
;================================================================
;	MAIN MENU
;================================================================
#include mod\MainMenu.ahk
Menu, MenuLevel6, % (pref_isize="s" ? "Check" : "Uncheck"), &small`t(16px)
Menu, MenuLevel6, % (pref_isize="m" ? "Check" : "Uncheck"), &medium`t(24px)
Menu, MenuLevel6, % (pref_isize="l" ? "Check" : "Uncheck"), &large`t(32px)
;================================================================
;	MAIN  GUI
;================================================================
guimw=595		; just for testing
Gui, 1:+LastFound +Resize +MinSize%guimw%x510
hMain := WinExist()
Gui, 1:Margin, 0, 0
Gui, 1:Font, s8, Tahoma
Gui, 1:Color, %menucolor%, %ctrlcolor%
Gui, 1:Show, w800 h600 Hide
if !hRB1 := RB_Create(hMain, "pT s0xA620")
	msgbox, Cannot create Rebar!
listW := listW ? listW : 150		; this has to go into ini options maybe
Gui, 1:Add, DDL, x10 y4 w%listW% h200 R20 +0x10000 hwndhThemes vtheme gseltheme,
;hThemes := CBE_Create("1", "", "2|2|" listW "|200", 0x800003, 0xA)
;CBE_Set(hSkins, "IL" hILs)

if !band1 := RB_Add(hRB1, hThemes, "Theme list", "", 0, "", "|200||", 0x4C4, 0)
	msgbox, Cannot add DropDownList to Rebar!
r := RB_Get(hRB1, "b" band1)			; get band1 borders
StringSplit, m, r, %A_Space%
tvw :=listW+ m1+m4
r := RB_Get(hRB1, "m")			; get band margins (XP+)
StringSplit, m, r, %A_Space%
tvw += m1+m4				; calculate TreeView width based on Combo width and Rebar borders+margins
r := RB_Get(hRB1, "b" band2)			; get band2 borders
StringSplit, m, r, %A_Space%

; TBSTYLE_TRANSPARENT/FLAT/WRAPABLE/TOOLTIPS/CCS_NODIVIDER/ADJUSTABLE/NOPARENTALIGH/NORESIZE
;hTB1 := TB_Create("1", "0|0|0|0", "0x9B60", "0x99")
hTB1 := TB_Create("1", "0|0|0|0", "0x996C", "0x99")
TB_SetIL(hTB1, "I0|" hILt%pref_isize%, 5)	; add D,H,P0 lists (Disabled, Hot, Pressed) space-separated
Loop, Parse, TBB, CSV
	{
	i1 := i2 := i3 := i4 := ""
	StringSplit, i, A_LoopField, |, %A_Space%
	i3 := i3!="" ? i3 : 0x10
	i4 := i4!="" ? i4 : 0x4
	TB_AddBtn(hTB1, "", A_Index, i4, i3, i1-1, 0)
;	TB_BtnSet(hTB1, A_Index, "t" i2)				; don't do it here coz it breaks b2w !!!
	}
TB_Set(hTB1, "b" 0x18|(0x18<<16) " d" 0x4|(0x8<<16))
b2w := TB_Get(hTB1, "sW")						; initial toolbar width
b2h := TB_Get(hTB1, "bH")						; initial toolbar height
if !band2 := RB_Add(hRB1, hTB1, "Toolbar", "", 0, b2w, b2h, 0x6C4, 0)	; use 0x4C4 or 0xCC4
	msgbox, Cannot add Toolbar to Rebar!
TB_Size(hTB1)
RB_Set(hRB1, band1, "ic" listW "|21|200|21|200")
RB_Set(hRB1, band2, "ic" b2w "|" b2h "|200|" b2h "|200")
;RB_Set(hRB1, band2, "ic" 620-tvw+ m1+m4 "|" b2h "|200|" b2h "|200")
RB_Size(hRB1)
;ControlGetPos,,, w,,, ahk_id %hRB1%
h := RB_Get(hRB1, "h"), w := RB_GetBand(hRB1, band2, "w")
;msgbox, h=%h% b2h=%b2h%
if !hBack := ResizeBmp(hSkin, w, h)
	msgbox, We don't have hBack
RB_Set(hRB1, band1, "bk" hBack)
RB_Set(hRB1, band2, "bk" hBack)
;TB_Set(hTB1, "s0x9B64 e0x99")
;TB_Set(hTB1, "s0x996C e0x99")
Loop, Parse, TBB, CSV
	{
	i2 := ""
	StringSplit, i, A_LoopField, |, %A_Space%
	TB_BtnSet(hTB1, A_Index, "t" i2)
	}

tsy := h+4
Gui, 1:Add, TreeView, x0 y%tsy% w%tvw% h400 ImageList%hILts% -Buttons -Lines +0x9A30 +E0x10 vTV1 gseltv,
clrH := lblH := dftH := oriH := ""		; label handles for use in msmove()
Gui, Add, Tab2, x%tvw% y%tsy% w430 h415 -Theme -Wrap AltSubmit +0x400114A hwndhTab vtab gtabchange
	, Details|Preview	;|Editor
DllCall("SendMessage" AW, Ptr, hTab, "UInt", 0x1334, "UInt", 1, "UInt", 1)	; TCM_SETEXTENDEDSTYLE
Gui, Tab, 1
i=1
Loop, 20
	{
	Gui, 1:Add, Text, x%tvw% y%tsy% w120 h18 +0x200 Right Hidden hwndhLbl%A_Index% vlbl%A_Index%, 
	lblH .= hLbl%A_Index% ","
	Gui, 1:Add, Button, x+1 w18 h18 +0x8040 Hidden hwndhSel%A_Index% vsel%A_Index% gsss, 
	SendMessage, 0xF7, 1, hIselN,, % "ahk_id " hSel%A_Index%	; BM_SETIMAGE, IMAGE_ICON
	selH .= hSel%A_Index% ","
	Gui, 1:Add, Edit, x+1 w230 h18 Hidden hwndhEdt%A_Index% vedt%A_Index% gsse, 
	if A_Index in 3,4,9,10,11,12,13,15,17,18,19,20
		GuiControl, 1:+Number, edt%A_Index%
	if A_Index in 5,6,7,14,16
		{
		Gui, 1:Add, ListView, x+1 w18 h18 -E0x200 +Border -hdr +ReadOnly Hidden BackgroundBlack AltSubmit hwndhClr%i% vLV%i% gseltask,
		clrH .= hClr%i% ","
		Gui, 1:Add, Button, x+1 w18 h18 +0x8040 Hidden hwndhDft%A_Index% vdft%A_Index% gssd, 
		i++
		}
	else
		Gui, 1:Add, Button, x+20 w18 h18 +0x8040 Hidden hwndhDft%A_Index% vdft%A_Index% gssd, 
	dftH .= hDft%A_Index% ","
	SendMessage, 0xF7, 1, hIdft,, % "ahk_id " hDft%A_Index%	; BM_SETIMAGE, IMAGE_ICON
	Gui, 1:Add, Button, x+1 w18 h18 +0x8040 Hidden hwndhOri%A_Index% vori%A_Index% gssr, 
	oriH .= hOri%A_Index% ","
	SendMessage, 0xF7, 1, hIori,, % "ahk_id " hOri%A_Index%	; BM_SETIMAGE, IMAGE_ICON
	}
Gui, 1:Font, s7, Tahoma
Gui, 1:Add, Button, x538 w18 h18 Hidden hwndhBTask vbtntask gseltask, �
Gui, Tab,,
Gui, 1:Add, Edit, x%tvw% y455 w430 h35 -E0x200 -0x200040 ReadOnly Hidden hwndhDtl vdetails,
SendMessage, 0xD3, 3, 0x50005,, ahk_id %hDtl%	; EM_SETMARGINS, EC_LEFTMARGIN|EC_RIGHTMARGIN
Gui, 1:Font, s8, Tahoma
Gui, Tab, 2
Gui, 1:Add, Picture, x%tvw% y%tsy% w430 h300 Section 0x10E Border hwndhPbk vPbk, ;skin.bmp
Gui, 1:Add, Text, % "x" tvw+4 " yp+2 w150 h16 +0x200 BackgroundTrans Hidden vnotif", ?
Gui, 1:Add, Text, % "x" tvw+4 " ys+hs-36 w120 h16 +0x200 BackgroundTrans Hidden vwarnZoom", ?
Gui, 1:Add, Text, % "x" tvw+4 " y+2 w170 h16 +0x200 BackgroundTrans Hidden vwarnRsz", ?
Gui, 1:Add, Picture, % "x" tvw+115 " y" tsy+50 " w200 h200 0x10E BackgroundTrans hwndhPv1 vPv1",
Gui, 1:Add, Text, xp-1 yp-1 wp+2 hp+2 0x7 BackgroundTrans Hidden hwndhFr vFrame,
Gui, 1:Add, Text, x%tvw% y360 w430 h40 -Border vdebugT,
Gui, 1:Add, Checkbox, xp y+2 h16 Section Checked%cntall% vcntall gtoggleCount2, Show all images in the strip
Gui, 1:Add, Edit, x+5 yp-1 h18 w45 Disabled%cntall% veidx gsetIndex, 1
Gui, 1:Add, UpDown, Wrap Disabled%cntall% viidx gtoggleCount, 1
Gui, 1:Add, Checkbox, xs y+-1 h16 Checked%showframe% vshowframe gtoggleFrame, Show image frame
Gui, 1:Add, Checkbox, xp y+1 h16 Checked%ppa% vppa gtogglePPA, Use per-pixel alpha (32bit only)
Gui, 1:Add, Slider, xs+200 ys w190 h30 -AltSubmit Range0-255 Tooltip TickInterval16 hwndhOpaq vopaq gsetAlpha, %opaq%
Gui, 1:Font, s7, Tahoma
Gui, 1:Add, Text, x+1 yp w35 hp Center valphaB2, Alpha %opaq%
Gui, 1:Font, s8, Tahoma
Gui, 1:Add, Checkbox, xs+200 y+3 w190 h16 Checked%forceT% vforceT gsetTrans, Use custom transparency color key
Gui, 1:Add, ListView, x+1 yp-2 w38 h18 -E0x200 +Border -hdr +ReadOnly Background%forceTC% AltSubmit hwndhFTC vforceTC gselTransClr,
GuiControl, % (forceT ? "Show" : "Hide"), forceTC
pvCtrls := "cntall,eidx,iidx,showframe,ppa,alphaB2,opaq,forceT,forceTC"	; preview controls to show/hide
GuiControlGet, Pbk, 1:Pos, Pbk
hBrush := checkBrush(pvsq, pvclr1, (s2Clr ? pvclr2 : ""))
hBmPbk := setBrush(hPbk, hBrush)
Gui, 1:Font, bold c%ntfclr%
GuiControl, Font, notif
Gui, 1:Font, bold c%zfclr%
GuiControl, Font, warnZoom
Gui, 1:Font, bold c%nmclr%
GuiControl, Font, warnRsz
Gui, 1:Font, Norm cDefault

Gui, Tab, 3,
Gui, Add, Text, x%tvw% y%tsy% w430 h300 Section 0x4 Border hwndhPEd vPEd,
Gui, Tab,,
StringTrimRight, clrH, clrH, 1
StringTrimRight, lblH, lblH, 1
StringTrimRight, selH, selH, 1
StringTrimRight, dftH, dftH, 1
StringTrimRight, oriH, oriH, 1
tvbase := TV_Add("no skin.ini", 0, "Icon33")						; script icon
list := inilist1
Loop, Parse, sections, CSV
	{
	idx := A_Index
	tvi%idx% := TV_Add(A_LoopField, tvbase, "Icon34")	; red folder icon
	if idx>1
		{
		StringLeft, r, defaults%idx%, 3						; transparency, group, states
		StringTrimLeft, defaults%idx%, defaults%idx%, 3
		ArrayPut("_Default", tvi%idx% "_D", r)					; store bmp data
		defaults%idx% .= defaults0
		list := inilist2
		}
	Loop, Parse, defaults%idx%, CSV
		{
		v2=
		StringLeft, t, A_LoopField, 1							; get data type
		ArrayPut("_Default", tvi%idx% "_T" A_Index, t)			; store data type
		StringTrimLeft, v, A_LoopField, 1						; get value
		if (v="")
			continue
		StringSplit, v, v, |									; get range, if any
		ArrayPut("_Default", tvi%idx% "_V" A_Index, v1)		; store value
		ArrayPut("_Default", tvi%idx% "_R" A_Index, v2)		; store range
		}
	Loop, Parse, list, CSV
		ArrayPut("_Default", tvi%idx% "_L" A_Index, A_LoopField)	; store field names
	}
gosub listth
GuiControl, 1:Choose, theme, 1						; preselect first theme in the list
gosub seltheme
Gui, 1:Show, AutoSize Hide
Gui, 1:Show, Center, %appname%
GroupAdd, limited, ahk_id %hMain%

WPrc1 := RegisterCallback("easymove", "F", 4, "")	; hook WS_LBUTTONDOWN
gosub buildSkinSel
;================================================================
hCursM := DllCall("LoadCursor", Ptr, 0, "Int", 32646, Ptr)	; IDC_SIZEALL
hCursH := DllCall("LoadCursor", Ptr, 0, "Int", 32649, Ptr)	; IDC_HAND
hCursL := DllCall("LoadCursor", Ptr, 0, "Int", 32651, Ptr)	; IDC_HELP
OnMessage(0x200, "msmove")						; WM_MOUSEMOVE
OnMessage(0x47, "ZoomUpdate")						; WM_WINDOWPOSCHANGED
OnMessage(0x4E, "notif")							; WM_NOTIFY
OnMessage(0x111, "cmnd")							; WM_COMMAND
ZoomStore(1, hMain)
;nWP := RegisterCallback("WProc", "F", 4, "")
;oWP := DllCall("SetWindowLong", Ptr, hMain, "Int", -4, Ptr, nWP, Ptr)
i := "initAbout"
if IsLabel(i)
	gosub %i%
init=1											; Avoids unnecessary object deletion on exit
OnExit, exit
return
;================================================================
;	END OF AUTOEXEC SECTION
;================================================================
reload:
Reload
Sleep, 3000

GuiClose:
saveandexit:
if !init
	ExitApp
if !abnormaluse
	gosub change
sleep, 200
if change
	{
	MsgBox, 0x2034, RP Theme Editor warning, There's unsaved work in the following themes:`n%change%`n`nExit without saving?
	IfMsgBox No
		return
	}
OnMessage(0x47, "")						; WM_WINDOWPOSCHANGED
OnMessage(0x4E, "")						; WM_NOTIFY
OnMessage(0x111, "")						; WM_COMMAND
OnMessage(0x200, "")						; WM_MOUSEMOVE
ExitApp

exit:
if !init
	ExitApp
zoom=
DllCall("DeleteObject", Ptr, hbmpv)
DllCall("DeleteObject", Ptr, hBrush)
DllCall("DeleteObject", Ptr, hBmPbk)
DllCall("DeleteObject", Ptr, hSBrush)			; created in Settings
DllCall("DeleteObject", Ptr, hSBmPbk)		; created in Settings
DllCall("DeleteObject", Ptr, hSTBrush)		; created in Settings
DllCall("DeleteObject", Ptr, hAbtBrush)
Loop, Parse, sections, CSV
	{
	i := tvi%A_Index%
	if hBm%i%
		DllCall("DeleteObject", Ptr, hBm%i%)
	}
if RB1
	RB_Cleanup(hRB1)	; no need for TB_Cleanup(hTB1), the Rebar will delete all child windows in its bands
					; Also no need for RB_Destroy() since AHK will destroy the Rebar itself
; delete stray bitmap/icon/cursor handles, ImageLists etc.
;DllCall("DestroyCursor", Ptr, hCursH); Don't destroy stock objects !
;DllCall("DestroyCursor", Ptr, hCursM)
;DllCall("DestroyCursor", Ptr, hCursL)
DllCall("DestroyIcon", Ptr, hIselN)
DllCall("DestroyIcon", Ptr, hIselY)
DllCall("DestroyIcon", Ptr, hIdft)
DllCall("DestroyIcon", Ptr, hIori)
DllCall("DeleteObject", Ptr, hSkin)
DllCall("DeleteObject", Ptr, hBack)
Loop, %skincount%
	DllCall("DeleteObject", Ptr, hDSkin%A_Index%)
IL_Destroy(hILs)
IL_Destroy(hILts)
IL_Destroy(hILtm)
IL_Destroy(hILtl)
IL_Destroy(hILte)
IL_Destroy(skin)
FileRemoveDir, %A_Temp%\RPTE, 1
IniWrite, %mnuvis%, %appini%, Preferences, ShowMenu
IniWrite, %tbvis%, %appini%, Preferences, ShowToolbar
IniWrite, %pref_isize%, %appini%, Preferences, IconSize
IniWrite, %skinSrc%, %appini%, Preferences, ExternalSkin
IniWrite, %skinPath%, %appini%, Preferences, ExternalSkinPath
IniWrite, %skinsz%, %appini%, Preferences, SkinW
IniWrite, %tSkin%, %appini%, Preferences, SkinType
IniWrite, %cSkin%, %appini%, Preferences, SkinColor
IniWrite, %ctrlcolor%, %appini%, Preferences, CtrlColor
IniWrite, %bst%, %appini%, Preferences, TooltipBalloon
IniWrite, %pref_ttdelay%, %appini%, Preferences, TooltipDelay
IniWrite, %pref_ttshow%, %appini%, Preferences, TooltipShow

IniWrite, %zf%, %appini%, Zoom, ZoomFactor
IniWrite, %maxzoom%, %appini%, Zoom, ZoomMax
IniWrite, %zr%, %appini%, Zoom, ZoomRound
IniWrite, %zi%, %appini%, Zoom, ZoomInvert
IniWrite, %zs%, %appini%, Zoom, ZoomShadow
IniWrite, %zw%, %appini%, Zoom, ZoomWindow
IniWrite, %maxzw%, %appini%, Zoom, ZoomWindowMax

IniWrite, %showframe%, %appini%, PreviewItem, ShowFrame
IniWrite, %cntall%, %appini%, PreviewItem, ShowAllImgs
IniWrite, %forceT%, %appini%, PreviewItem, ForceTrans
IniWrite, %forceTC%, %appini%, PreviewItem, ForceTransColor
IniWrite, %aa%, %appini%, PreviewItem, GlobalAlpha
IniWrite, %ppa%, %appini%, PreviewItem, PerPixelAlpha

IniWrite, %pvsq%, %appini%, PreviewItem, CheckeredSquare
IniWrite, %s2Clr%, %appini%, PreviewItem, UseTwoColors
IniWrite, %pvclr1%, %appini%, PreviewItem, CheckeredColor1
IniWrite, %pvclr2%, %appini%, PreviewItem, CheckeredColor2
IniWrite, %ntfclr%, %appini%, PreviewItem, ZoomNotifColor
IniWrite, %zfclr%, %appini%, PreviewItem, ZoomFactorColor
IniWrite, %nmclr%, %appini%, PreviewItem, NotificationColor
ExitApp
;================================================================
buildSkinSel:											; Create GUI3 for Skin/Grid selection
;================================================================
Gui, 3:Font, s8 Bold cBlack, Tahoma
Gui, 3:Margin, 5, 5
Gui, 3:Color, White, White
Gui, 3:-Caption +ToolWindow +Border +AlwaysOnTop
Gui, 3:Add, Text, x4 y4 w62 h62 0x7 Border BackgroundTrans,
Gui, 3:Add, Text, x4 y4 w62 h62 0x8 BackgroundTrans,
Loop, %skincount%
	Gui, 3:Add, Picture, % "x" 5+65*(A_Index-1-8*((A_Index-1)//8)) " y" 5+65*((A_Index-1)//8) " w60 h60 0xE gselskinimg vimage" A_Index " hwndhSkinImg" A_Index,
Gui, 3:Show, AutoSize Hide, Select skin image
WinGet, hGui3, ID, Select skin image
hGui3+=0
oWP%hGui3% := DllCall("SetWindowLong", Ptr, hGui3, "Int", -4, Ptr, WPrc1, Ptr)
return
;================================================================
change:
;================================================================
ControlGet, ths, List,, ComboBox1, ahk_id %hMain%
if !ths
	return
change=
Loop, Parse, ths, `n
	{
	if !ArrayGet(A_LoopField)
		continue
	i := A_LoopField
	Loop, Parse, sections, CSV		; just to be on the safe side, in case sections ever get changed
		{
		v := ArrayGet(i, tvi "_N" A_Index)				; Get value from New array
		o := ArrayGet(i, tvi "_O" A_Index)				; Get value from Original array
;		sws%A_Index% := ArrayGet(i, tvi "_S" A_Index)	; Get switch
		if (v!=o)
			{
			change .= i "`n"
			break
			}
		}
	}
StringTrimRight, change, change, 1
; Each theme needs a switch to flag whether it's been already saved manually
; The flag must be set when saving (or saving as) and reset upon any subsequent change
; Here we must check that flag to avoid superfluous operations
return
;================================================================
GuiSize:
;================================================================
RB_Show(hRB1, band1, tbvis)
RB_Show(hRB1, band2, tbvis)
ny := RB_Get(hRB1, "h")+4
_l := ny, AGuiW := A_GuiWidth, AGuiH := A_GuiHeight
GuiControl, % "1:" (ny != ony ? "MoveDraw" : "Move"), TV1, % "y" ny " h" AGuiH-ny
GuiControl, 1:MoveDraw, details, % "y" AGuiH-35
;if (A_GuiWidth=oagw && ny=ony)
if (ny=ony)
	return
oagw := A_GuiWidth, ony := ny
GuiControlGet, TVp, 1:Pos, TV1
GuiControl, 1:Move, tab, % "y" TVpY
GuiControl, 1:MoveDraw, Pbk, % "y" ny
GuiControlGet, Pbk, 1:Pos, Pbk
_l_ := PbkY+PbkH
GuiControl, 1:MoveDraw, PEd, % "y" ny
GuiControl, 1:MoveDraw, notif, % "y" ny+2
GuiControl, 1:MoveDraw, warnRsz, % "y" _l_-19
GuiControl, 1:MoveDraw, warnZoom, % "y" _l_-37
GuiControl, 1:MoveDraw, debugT, % "y" _l_+1
GuiControlGet, dT, 1:Pos, debugT
_l_ += dTH+3
GuiControl, 1:MoveDraw, cntall, % "y" _l_
GuiControl, 1:MoveDraw, eidx, % "y" _l_-1
GuiControl, 1:MoveDraw, iidx, % "y" _l_-1
GuiControl, 1:MoveDraw, showframe, % "y" _l_+17
GuiControl, 1:MoveDraw, ppa, % "y" _l_+34
GuiControl, 1:MoveDraw, opaq, % "y" _l_-1
GuiControl, 1:MoveDraw, alphaB2, % "y" _l_-1
GuiControl, 1:MoveDraw, forceT, % "y" _l_+34
GuiControl, 1:MoveDraw, forceTC, % "y" _l_+32

i=1
Loop, 20
	{
	_ := _l+19*(A_Index-1)
	GuiControl, 1:MoveDraw, lbl%A_Index%, % "y" _
	GuiControl, 1:MoveDraw, sel%A_Index%, % "y" _
	GuiControl, 1:MoveDraw, edt%A_Index%, % "y" _
	GuiControl, 1:MoveDraw, dft%A_Index%, % "y" _
	GuiControl, 1:MoveDraw, ori%A_Index%, % "y" _
	if A_Index in 5,6,7,14,16
		{
		GuiControl, 1:Move, LV%i%, % "y" _
		i++
		}
	}
if tab=2
	gosub toggleCount
if zoom
	{
	zoom=0
	SetTimer, zoomon, -1
	}
return
;================================================================
;	HOTKEYS
;================================================================
#IfWinActive, ahk_group limited

^]::
ListVars
return
;================================================================
;	ZOOM-RELATED HOTKEYS
;================================================================
!Z::
zoom:
zoom := !zoom
TB_BtnSet(hTB1, 18, "s" (zoom ? 0x2 : 0x4))	; TBSTATE_PRESSED / ENABLED
GuiControl,, warnZoom, %zType% %zf%X
GuiControl, % (zoom ? "Show" : "Hide"), warnZoom
if zoom
	SetTimer, zoomon, -1
return

NumpadAdd::
if (zf = maxzoom)
	return
zf++
GuiControl,, warnZoom, %zType% %zf%X
GuiControl,, notif, Increasing zoom to %zf%X
gosub notifyPvOn
if !zoom
	return
zoom=0
SetTimer, zoomon, -1
return

NumpadSub::
if zf = 1
	return
zf--
GuiControl,, warnZoom, %zType% %zf%
GuiControl,, notif, Decreasing zoom to %zf%X
gosub notifyPvOn
if !zoom
	return
zoom=0
SetTimer, zoomon, -1
return

NumpadMult::
zr := zr ? "" : "R"
GuiControl,, notif, % (zr ? "Round" : "Square") " magnifier"
gosub notifyPvOn
if !zoom
	return
zoom=0
SetTimer, zoomon, -1
return

NumpadDiv::
zi := zi ? "" : "I"
zType := zi ? "Inverted zoom" : "Zoom"
GuiControl,, notif, % "Inverted zoom " (zi ? "ON" : "OFF")
GuiControl,, warnZoom, %zType% %zf%X
gosub notifyPvOn
if !zoom
	return
zoom=0
SetTimer, zoomon, -1
return

^NumpadAdd::
if (zw = maxzw)
	return
zw+=10
GuiControl,, notif, Increasing size to %zw%px
gosub notifyPvOn
if !zoom
	return
zoom=0
SetTimer, zoomon, -1
return

^NumpadSub::
if zw = 50
	return
zw-=10
GuiControl,, notif, Decreasing size to %zw%px
gosub notifyPvOn
if !zoom
	return
zoom=0
SetTimer, zoomon, -1
return

#IfWinActive
;================================================================
zoomon:
;================================================================
Zoom(hPv1, zf zr zs zi, zw)	; zf=zoom factor, zr=round shape, zs=shadow, zi=invert; zw=window size
return
;================================================================
notifyPvOn:
GuiControl, Show, notif
SetTimer, notifyPvOff, -1500
return
notifyPvOff:
GuiControl, Hide, notif
return
;================================================================
moveit:
;================================================================
DllCall("SetCursor", Ptr, hCursM)
PostMessage, 0xA1, 2,,, A	; WM_NCLBUTTONDOWN
return
;================================================================
aot:
;================================================================
aot := !aot
Gui, % "1:" (aot ? "+" : "-") "AlwaysOnTop"
Menu, Tray, % (aot ? "Check" : "Uncheck"), Always on top
Menu, MenuLevel5, % (aot ? "Check" : "Uncheck"), Always &on top
return
;================================================================
toggleMnu:
;================================================================
mnuvis := !mnuvis
Menu, MenuLevel4, Rename, % (mnuvis ? "Show" : "Hide") " &menu", % (mnuvis ? "Hide" : "Show") " &menu"
Menu, Tray, Rename, % (mnuvis ? "Show" : "Hide") " main menu", % (mnuvis ? "Hide" : "Show") " main menu"
Gui, 1:Menu, % (mnuvis ? "Main" : "")
return
;================================================================
toggleTB:
;================================================================
tbvis := !tbvis
RB_Show(hRB1, band1, tbvis)
RB_Show(hRB1, band2, tbvis)
Menu, MenuLevel4, Rename, % (tbvis ? "Show" : "Hide") " &toolbar", % (tbvis ? "Hide" : "Show") " &toolbar"
Menu, Tray, Rename, % (tbvis ? "Show" : "Hide") " toolbar", % (tbvis ? "Hide" : "Show") " toolbar"
return
;================================================================
selfolder:
;================================================================
FileSelectFolder, i, *%tfpath%, 2, Select a folder to load themes from:
if (!i OR ErrorLevel)
	return
tfpath := i
SetTimer, listth, -1
return
;================================================================
listth:
;================================================================
sl=
Loop, %tfpath%\skin*.ini, 0, 1
	{
	sp := A_LoopFileDir						; get dir path
	StringSplit, j, A_LoopFileLongPath, \
	j0--
	sn := j%j0%							; get theme folder name
	StringReplace, i, A_LoopFileName, skin,,
	StringReplace, i, i, .ini,,
	i=%i%								; find if skin ini name is variant
	sl .= sn (i ? " `: " i : "")					; put together theme name and variant, if any
	ArrayPut(sn, "path" i, sp)				; save dir path to array
	sl .= "|"								; complete the listing
;	CBE_Add(hThemes, "ii1|si2|oi1|id-1|tx" sn (i ? " `: " i : ""))
	}
StringTrimRight, sl, sl, 1
Sort, sl, CL D|
GuiControl, 1:, theme, |%sl%
GuiControl, 1:Choose, theme, 1				; preselect first theme in the list
return
;================================================================
seltheme:
;================================================================
Gui, 1:Submit, NoHide
if (!theme OR theme=oldtheme)
	return
GuiControl, Hide, details
;Control, Disable,,, ahk_id %hRB1%
;GuiControl, 1:-Redraw, TV1
;GuiControl, 1:Disable, TV1
t2 := ""
StringSplit, t, theme, :, %A_Space%
inifile := ArrayGet(t1, "path" t2) "\skin" t2 ".ini"
thini := ArrayGet(theme, "loaded")						; find out if theme is already in the array
Loop, Parse, sections, CSV
	{
	idx := A_Index
	ctvi := tvi%idx%
if debug
	FileAppend, index %idx% as %A_LoopField% [ctvi=%ctvi%], %dbgfile%
	if !thini										; if not in the array, add all items
		{
		cs := mcs := A_LoopField					; use mcs only for detection in initarray below
		idx2=0
		items := cs="Global" ? inilist1 : inilist2
		if (cs="StartButton")
			{
			if i := TV_GetChild(ctvi)
				{
				TV_Modify(i, "Icon34")
				Loop, 10
					if !i := TV_GetNext(i)
						break
					else TV_Modify(i, "Icon34")
				TV_Modify(ctvi, "-Expand")
				}
			lang=
			if s := IniFindSubSections(inifile, cs, buf)
			Loop, Parse, buf, `n
				{
				ss2=
				cs := A_LoopField
tooltip, cs=%cs%
				StringSplit, ss, cs, .
				if (ss2 && !tvil%ss2%)
					{
					lang := FindLang(ss2, "N")
					ctvi := tvil%ss2% := TV_Add(lang, tvi%idx%, "Icon35")	; add flag icon !!!
					Loop, Parse, defaults%idx%, CSV
						{
						if (A_LoopField="")
							continue
						v2=
						StringLeft, t, A_LoopField, 1				; get data type
						StringTrimLeft, v, A_LoopField, 1			; get value
						StringSplit, v, v, |						; get range, if any
						ArrayPut("_Default", ctvi "_T" A_Index, t)	; store data type
						ArrayPut("_Default", ctvi "_V" A_Index, v1)	; store value
						ArrayPut("_Default", ctvi "_R" A_Index, v2)	; store range
						}
					Loop, Parse, inilist2, CSV
						ArrayPut("_Default", ctvi "_L" A_Index, A_LoopField)	; store field names
					}
				else if (ss2 && tvil%ss2%)
					ctvi := tvil%ss2%
tooltip, gosub initarray
				gosub initarray
tooltip, returned initarray
				TV_Modify(ctvi, "Icon" (idx2 ? "35" : "34"))
				}
			TV_Modify(tvi%idx%, (s>1 ? "Expand" : "-Expand"))
			}
		else gosub initarray
tooltip, returned initarray 1
		}
	else
		{
		tooltip, gosub buildBmp
		gosub buildBmp
		tooltip, returned buildBmp
		if (cs="StartButton")
			{
			if i := TV_GetChild(ctvi)
				{
				ctvi := i
				TV_Modify(ctvi, "Icon35")
				gosub buildBmp
				Loop, 10
					{
					if !ctvi := TV_GetNext(ctvi)
						break
					TV_Modify(ctvi, "Icon35")
		tooltip, gosub buildBmp %A_Index%
					gosub buildBmp
		tooltip, return buildBmp %A_Index%
					}
				TV_Modify(tvi%idx%, "Expand")	; use tvi%idx% to acces parent branch!
				}
			}
		}
	}
ArrayPut(theme, "loaded", 1)
oldtheme := theme
GuiControl, 1:Enable, TV1
GuiControl, 1:+Redraw, TV1
TV_Modify(tvbase, "VisFirst Select Expand", "skin" t2 ".ini")
Control, Enable,,, ahk_id %hRB1%
return
;================================================================
initarray:
;================================================================
Loop, Parse, items, CSV
	{
	IniRead, i, %inifile%, %cs%, %A_LoopField%, %A_Space%
	if j := InStr(i, "`;")								; If there's comment on the same row
		StringLeft, i, i, j-1							; remove it since XP is dumb and won't
	i=%i%										; Clean any remaining whitespace
	if (i != "")
		idx2++									; Count existing items (values)
	ArrayPut(theme, ctvi "_O" A_Index, i)				; create array of Original values
	ArrayPut(theme, ctvi "_N" A_Index, i)				; initialize New array with original values
	ArrayPut(theme, ctvi "_S" A_Index, (i!="" ? 1 : 0))	; initialize item presence switch
	}
if (mcs!="Global" && (i := ArrayGet(theme, ctvi "_O1")	))	; Get bitmap path
	{
	bmpfile := isPathRelative(i) ? PathCombine(inifile, i) : i	; and make it full if relative
	if !bmpcnt := ArrayGet(theme, ctvi "_O2")			; Get image count,
		bmpcnt := ArrayGet("_Default", ctvi "_V2")		; or fall back to default if blank
	FileRead, bmphdr, *m54 *c %bmpfile%				; Read bitmap header
	bmW := NumGet(bmphdr, 18, "UInt")				; Get bitmap width
	bmH := Abs(NumGet(bmphdr, 22, "Int"))			; Get bitmap height (can be negative)
	bmbpp := NumGet(bmphdr, 28, "UShort")			; bpp (must be read for each bitmap and checked)
	isz := bmH//bmpcnt								; Find real individual image size
	epm := ArrayGet(theme, ctvi "_O12")				; Get custom premultiply alpha level
	hBm%ctvi% := LoadBmp(bmpfile, bmW, bmH, bmbpp, epm)	; ext premult value, if available
	ArrayPut(theme, ctvi "_BMw", bmW)				; Store bitmap width
	ArrayPut(theme, ctvi "_BMh", bmH)				; Store bitmap height
	ArrayPut(theme, ctvi "_BMc", bmbpp)				; Store bitmap color depth
	ArrayPut(theme, ctvi "_BMsz", isz)					; Store real image size
	}
ArrayPut(theme, ctvi "_C" idx, idx2)					; store count of items in current section
TV_Modify(ctvi, "Icon" (idx2 ? "35" : "34"))
return
;================================================================
buildBmp:
idx2 := ArrayGet(theme, ctvi "_C" idx)					; find out if section exists and has items
TV_Modify(ctvi, "Icon" (idx2 ? "35" : "34"))
if hBm%ctvi%
	if DllCall("DeleteObject", Ptr, hBm%ctvi%)			; Delete all previous bitmap handles
		hBm%ctvi%=
if (mcs != "Global" && idx2 && (i := ArrayGet(theme, ctvi "_N1")))	; Get bitmap path
	{
	bmpfile := isPathRelative(i) ? PathCombine(inifile, i) : i	; and make it full if relative
	if !bmpcnt := ArrayGet(theme, ctvi "_N2")		; Get image count
		bmpcnt := ArrayGet("_Default", ctvi "_V2")
	bmW := ArrayGet(theme, ctvi "_BMw")			; Get image width
	bmH := ArrayGet(theme, ctvi "_BMh")			; Get image height
	bmbpp := ArrayGet(theme, ctvi "_BMc")		; Get image color depth
	isz := bmH//bmpcnt							; Find real individual image size
	epm := ArrayGet(theme, ctvi "_N12")			; Get custom premultiply alpha level
	hBm%ctvi% := LoadBmp(bmpfile, bmW, bmH, bmbpp, epm)	; ext premult value, if available
	}
return
;================================================================
seltv:
;================================================================
e := A_GuiEvent
tvi := A_EventInfo
if (lasttv = tvi OR e != "S")
	return
if (tvi=tvbase)
	{
	GuiControl, 1:Hide, tab
	DllCall("InvalidateRect", Ptr, hThemes, Ptr, 0, "UInt", 1)
	DllCall("InvalidateRect", Ptr, hTB1, Ptr, 0, "UInt", 1)
	return
	}
GuiControl, 1:Choose, tab, % (tab=3 && tvi=tvi1 ? (lasttab != 3 ? lasttab : 1) : tab ? tab : 1)
GuiControl, 1:Show, tab
DllCall("InvalidateRect", Ptr, hThemes, Ptr, 0, "UInt", 1)
DllCall("InvalidateRect", Ptr, hTB1, Ptr, 0, "UInt", 1)
lasttv := tvi, lasttab := tab
;GuiControl, % (tvi!=tvbase ? "Show" : "Hide"), details
Loop, 20
	{
	v := ArrayGet(theme, tvi "_N" A_Index)				; Get value from New array
	o := ArrayGet(theme, tvi "_O" A_Index)				; Get value from Original array
	sws%A_Index% := ArrayGet(theme, tvi "_S" A_Index)	; Get switch
	SendMessage, 0xF7, 1, % (sws%A_Index% ? hIselY : hIselN),, % "ahk_id " hSel%A_Index%	; BM_SETIMAGE, IMAGE_ICON
	b%A_Index% := ArrayGet("_Default", tvi "_D" A_Index)	; Get default bitmap data
	t%A_Index% := ArrayGet("_Default", tvi "_T" A_Index)	; Get default type
	d := ArrayGet("_Default", tvi "_V" A_Index)				; Get default value
	l := ArrayGet("_Default", tvi "_L" A_Index)				; Get default label for current item
	GuiControl, 1:, lbl%A_Index%, %l% :					; Set corresponding field label
	GuiControl, 1:-g, edt%A_Index%						; We need this to avoid multiple calls
	GuiControl, 1:, edt%A_Index%, %v%					; to the 'sse' label which can
	Gui, 1:Submit, NoHide								; screw things up, so we just reenable it
	GuiControl, 1:+gsse, edt%A_Index%					; after setting the value in the Edit field
	GuiControl, % "1:" (sws%A_Index% ? "Enable" : "Disable"), edt%A_Index%
	GuiControl, % "1:" (tvi!=tvbase && l ? "Show" : "Hide"), lbl%A_Index%
	GuiControl, % "1:" (tvi!=tvbase && l ? "Show" : "Hide"), sel%A_Index%
	GuiControl, % "1:" (tvi!=tvbase && l ? "Show" : "Hide"), edt%A_Index%
	GuiControl, % "1:" (tvi!=tvbase && l && o!=v ? "Show" : "Hide"), ori%A_Index%
	GuiControl, % "1:" (tvi!=tvbase && l && d!= "" && d!=v ? "Show" : "Hide"), dft%A_Index%
	if (tvi=tvi1)
		SetLVcolor(A_Index, v)
;msgbox, tvi=%tvi% tvbase=%tvbase%`nlbl=%lbl% l=%l%`nv=%v% d=%d% o=%o%
	}
;================================================================
refreshData:
gosub isBadBmp
if (tvi=tvi1)									; We have no bitmap in Global section
	{
	GuiControl, 1:, debugT,
	GuiControl, 1:Hide, Frame
	GuiControl, 1:Hide, Pv1
	Loop, Parse, pvCtrls, CSV
		GuiControl, 1:Hide, %A_LoopField%
	GuiControl, 1:, warnRsz, No global preview yet
	GuiControl, 1:Show, warnRsz
	return
	}
i := ArrayGet(theme, tvi "_N1")					; Get bitmap path here so we can display it below
bmpfile := isPathRelative(i) ? PathCombine(inifile, i) : i	; and make it full if relative
dmsg := InStr(FileExist(bmpfile), "D") ? "No image has been assigned" : !hBm%tvi% ? "Assigned image is missing" : ""
if dmsg
	{
	GuiControl, 1:, debugT, %bmpfile%
	GuiControl, 1:Hide, Frame
	GuiControl, 1:Hide, Pv1
	Loop, Parse, pvCtrls, CSV
		GuiControl, 1:Hide, %A_LoopField%
	GuiControl, 1:, warnRsz, %dmsg%
	GuiControl, 1:Show, warnRsz
	return
	}
else GuiControl, 1:Show, Pv1
; if controls hidden, then...
	Loop, Parse, pvCtrls, CSV
		if (A_LoopField="forceTC" && !forceT)
			continue
		else GuiControl, 1:Show, %A_LoopField%
if !bmpcnt := ArrayGet(theme, tvi "_N2")			; Get image count as modified by user
	bmpcnt := ArrayGet("_Default", tvi "_V2")		; or fall back to default value when not set
bmW := ArrayGet(theme, tvi "_BMw")				; Get image width
bmH := ArrayGet(theme, tvi "_BMh")				; Get image height
bmbpp := ArrayGet(theme, tvi "_BMc")				; Get image color depth
nopma := ArrayGet(theme, tvi1 "_N4")				; Get global premult alpha switch
isz := bmH//bmpcnt								; Find real individual image size
GuiControl, 1:Range1-%bmpcnt%, iidx
if (iidx > bmpcnt)
	GuiControl, 1:, iidx, 1
if tab=2
	goto toggleCount
return
;================================================================
isBadBmp:
;================================================================
if (tvi != tvi31 && tvi31 != TV_GetParent(tvi))			; if not main StartButton or a sublang
	{
	GuiControl, 1:Hide, details
	return
	}
if !cBmp := ArrayGet(theme, tvi "_N1")					; Get bitmap path for current start button
	{
	GuiControl, 1:Hide, details
	return
	}
hTsk := ArrayGet(theme, tvi1 "_N8")					; Get taskbar height
cTsk := ArrayGet(theme, tvi "_N2")					; Get bitmap count for current start button
dTsk := ArrayGet("_Default", tvi "_V2")					; Get default bitmap count for start button
if (sws2 && !cTsk)									; If count enabled and field value is null
	{
	GuiControl, 1:Show, details
	GuiControl, 1:, details, Please set a valid value for the Count field above!
	return
	}
else if !sws2										; If count field is disabled,
	cTsk := dTsk									; assume default value
m := (cTsk != dTsk) ? "Warning: non-default Count value! Default value is " dTsk : "Looking good so far!"
isz := ArrayGet(theme, tvi "_BMh")//cTsk				; Get real image size
hMax := hTsk+7									; Apparently 7 is the most it can take
bmpHmax := cTsk*hMax								; Maximum allowed bitmap height
bmpHdef := cTsk*hTsk								; Default bitmap height for current taskbar size
if (isz>hMax)
	{
	GuiControl, 1:Show, details
	GuiControl, 1:, details,
		(LTrim
		Start button bitmap is TOO TALL for the taskbar height (%isz%px vs max. %hMax%px). Please adjust either:
		� taskbar height to a value of %isz% (in Global section -> TaskBarSize)		or
		� bitmap height to a multiple of %cTsk% between %bmpHdef% and %bmpHmax% (default: %bmpHdef%) using an image editor
		)
	}
else if (isz<hTsk)
	{
	GuiControl, 1:Show, details
	GuiControl, 1:, details,
		(LTrim
		Start button bitmap is SMALLER than the current taskbar height (%isz%px vs %hTsk%px).
		Please check how it looks and see if it can be enlarged (using an image editor).
		Allowed bitmap height range is %bmpHdef% to %bmpHmax%px (as a multiple of %cTsk%).
		)
	}
else GuiControl, 1:, details,
	(LTrim
	 Current StartButton height is %isz%px.
	Allowed button range is %hTsk% to %hMax%px.
	Allowed bitmap height range is %bmpHdef% to %bmpHmax%px (as a multiple of %cTsk%).

	%m%
	)
return
;================================================================
toggleCount:
;================================================================
Gui, 1:Submit, NoHide
disz := cntall ? bmH : isz							; Find display size according to switch
bmY := cntall ? 0 : isz*(iidx-1)						; calculate starting height for displaying single image
imgW := (bmW <= PbkW) ? bmW : PbkW - 10
imgH := (disz <= PbkH) ? disz : PbkH - 10
imgX := PbkX + (PbkW//2 - imgW//2)
imgY := PbkY + (PbkH//2 - imgH//2)
if (imgW != bmW OR imgH != disz)
	{
	GuiControl, 1:, warnRsz, Image has been resized to fit
	GuiControl, 1:Show, warnRsz
	}
else GuiControl, 1:Hide, warnRsz
GuiControl, 1:MoveDraw, Pv1, % "x" imgX " y" imgY " w" imgW " h" imgH
if showframe
	{
	GuiControl, 1:MoveDraw, Frame, % "x" imgX-1 " y" imgY-1 " w" imgW+2 " h" imgH+2
	GuiControl, 1:Show, Frame
	}
i := ArrayGet(theme, tvi "_N9") & 255			; get original value
aa := (i != "" ? 255-i : 0)
GuiControl, 1:, opaq, % 255-aa					; alpha can't be larger than 255
GuiControl, 1:, alphaB2, % "Alpha " 255-aa
repaint:
DllCall("DeleteObject", Ptr, hbmpv)
trm := forceT ? "C" forceTC : (bmbpp < 32 ? "M" : "A" aa)	; transparency method (standard or custom)
hbmpv := DrawTransBmp("1:Pv1", hBm%tvi%, "sw" bmW " sh" disz " sy" bmY " dw" imgW " dh" imgH, trm, ppa)
hcbmp := hBm%tvi%
SetFormat, Integer, H
hcbmp+=0, hbmpv+=0
SetFormat, Integer, D
GuiControl, 1:, debugT, � %bmpfile%`n� %bmW%x%bmH%px %bmbpp%bpp, item height=%isz%px, count=%bmpcnt% ;hBmpSrc=%hcbmp%, hBmpView=%hbmpv%
if zoom
	{
	zoom=0
	SetTimer, zoomon, -1
	}
return
;================================================================
toggleCount2:
;================================================================
Gui, 1:Submit, NoHide
GuiControl, % "1:" (cntall ? "Disable" : "Enable"), eidx
GuiControl, % "1:" (cntall ? "Disable" : "Enable"), iidx
goto toggleCount
return
;================================================================
toggleFrame:
;================================================================
Gui, 1:Submit, NoHide
GuiControl, 1:MoveDraw, Frame, % "x" imgX-1 " y" imgY-1 " w" imgW+2 " h" imgH+2
GuiControl, % "1:" (showframe ? "Show" : "Hide"), Frame
if zoom
	{
	zoom=0
	SetTimer, zoomon, -1
	}
return
;================================================================
togglePPA:
;================================================================
Gui, 1:Submit, NoHide
goto repaint	;ToggleCount
return
;================================================================
setAlpha:
;================================================================
Gui, 1:Submit, NoHide
if (opaq=oldopaq)
	return
e := A_GuiEvent
aa := 255-opaq
oldopaq := opaq
GuiControl, 1:, alphaB2, Alpha %opaq%
if e=Normal
;	goto repaint	;ToggleCount
	SetTimer, repaint, -1	;ToggleCount
return
;================================================================
setTrans:
;================================================================
Gui, 1:Submit, NoHide
GuiControl, % "1:" (forceT ? "Disable" : "Enable"), opaq
GuiControl, % "1:" (forceT ? "Show" : "Hide"), forceTC
goto repaint	;ToggleCount
return
;================================================================
setIndex:
;================================================================
Gui, 1:Submit, NoHide
if eidx not between 1 and %bmpcnt%
	eidx := 1
GuiControl, 1:, iidx, %eidx%
return
;================================================================
tabchange:
;================================================================
Gui, 1:Submit, NoHide
Gui, 1:+LastFound
ControlGet, tab, Tab,, SysTabControl321
; things I have to do because the Tab control is stupid!
DllCall("InvalidateRect", Ptr, hThemes, Ptr, 0, "UInt", 1)
DllCall("InvalidateRect", Ptr, hTB1, Ptr, 0, "UInt", 1)
if tab=2
	{
	if (tvi != tvibase && tvi != tvi1)
		goto toggleCount
	}
return
;================================================================
sss:		; skin section select
ssd:		; skin section default
ssr:		; skin section restore original
sse:		; skin section edit
;================================================================
StringTrimLeft, row, A_GuiControl, 3
bl := A_ThisLabel
if bl=sss
	{
	sws%row% := !sws%row%
	ArrayPut(theme, tvi "_S" row, sws%row%)
	SendMessage, 0xF7, 1, % (sws%row%=TRUE ? hIselY : hIselN),, % "ahk_id " hSel%row%	; BM_SETIMAGE, IMAGE_ICON
	GuiControl, % "1:" (sws%row% ? "Enable" : "Disable"), edt%row%
	os := (ArrayGet(theme, tvi "_O" row)!="") ? TRUE : FALSE	; get original switch by value
	GuiControl, % "1:" (os != sws%row% ? "Show" : "Hide"), ori%row%
	GuiControl, 1:Focus, % (sws%row% ? "edt" row : "ori" row)
	if sws%row%
		SendMessage, 0xB1, 0, 0xFFFFFFFF,, % "ahk_id " hedt%row%	; EM_SETSEL (all text)
	if row<3								; Dynamically check start button size compliance
		gosub isBadBmp					; but only if Bitmap or Count have been changed
	}
else if bl=ssd
	{
	d := ArrayGet("_Default", tvi "_V" row)		; get default value
	o := ArrayGet(theme, tvi "_O" row)		; get original value
	sws%row% := d!="" ? TRUE : FALSE
	ArrayPut(theme, tvi "_S" row, sws%row%)
	SendMessage, 0xF7, 1, % (sws%row%=TRUE ? hIselY : hIselN),, % "ahk_id " hSel%row%	; BM_SETIMAGE, IMAGE_ICON
	GuiControl, 1:, edt%row%, %d%
	GuiControl, % "1:" (o != d ? "Show" : "Hide"), ori%row%
	GuiControl, % "1:" (d!="" ? "Enable" : "Disable"), edt%row%
	SetLVcolor(row, d)
	Gui, 1:Submit, NoHide
	}
else if bl=ssr
	{
	o := ArrayGet(theme, tvi "_O" row)		; get original value
	sws%row% := o!="" ? TRUE : FALSE
	ArrayPut(theme, tvi "_S" row, sws%row%)
	SendMessage, 0xF7, 1, % (sws%row%=TRUE ? hIselY : hIselN),, % "ahk_id " hSel%row%	; BM_SETIMAGE, IMAGE_ICON
	GuiControl, 1:, edt%row%, %o%
	GuiControl, 1:Hide, ori%row%
	GuiControl, % "1:" (o!="" ? "Enable" : "Disable"), edt%row%
	SetLVcolor(row, 0)
	Gui, 1:Submit, NoHide
;	ArrayPut(theme, tvi "_N" row, %o%)	; might not be needed
	}
else if bl=sse
	{
	Gui, 1:Submit, NoHide
	if (edt%row%="")
		{
		sws%row% := FALSE
		ArrayPut(theme, tvi "_S" row, sws%row%)
		SendMessage, 0xF7, 1, hIselN,, % "ahk_id " hSel%row%	; BM_SETIMAGE, IMAGE_ICON
		GuiControl, 1:Disable, edt%row%
		}
	else SetLVcolor(row, edt%row%)
	ArrayPut(theme, tvi "_N" row, edt%row%)
	d := ArrayGet("_Default", tvi "_V" row)		; get default value
	o := ArrayGet(theme, tvi "_O" row)		; get original value
	GuiControl, % "1:" (d != "" && d != edt%row% ? "Show" : "Hide"), dft%row%
	GuiControl, % "1:" (o != edt%row% ? "Show" : "Hide"), ori%row%
	i := ArrayGet(theme, tvi "_C" row)
	ArrayPut(theme, tvi "_C" row, (o="" && edt%row%!="" && sws%row% ? i+1 : i>0 && o!="" && (!sws%row% OR !edt%row%) ? i-1 : i))		; needs refined and checked thoroughly !!!
	if row<3								; Dynamically check start button size compliance
		gosub isBadBmp					; but only if Bitmap or Count have been changed
	}
; we MUST add a check/set for NULL sections (no items) to avoid saving empty sections to ini
; ArrayPut/Get(theme, tvi "_C" row, value) where value is zero for empty sections
if (tvi=tvi1)
	{
	if row != 4
		return
; A change in tvi1 row4 (global PremultAlpha) would affect ALL sections so bitmaps must be rebuilt !!!
	nopma := edt4 ? edt4 : 0		; use this as a global variable for NoPremultAlpha
	}
else if row in 1,2,9,10,12
	{
	i := ArrayGet(theme, tvi "_N12")
	epm := (i ? i : 0) | nopma		; Get custom premultiply alpha level
	hBm%ctvi% := LoadBmp(bmpfile, bmW, bmH, bmbpp, epm)	; ext premult value, if available
	goto refreshData
	}
return
;================================================================
SetLVcolor(r, cs)
{
Static l="5,6,7,14,16"
if r not in %l%
	return
StringSplit, c, cs, %A_Space%
ofi := A_FormatInteger
SetFormat, Integer, H
i := c3|c2<<8|c1<<16
SetFormat, Integer, %ofi%
Loop, Parse, l, CSV
	if (r=A_LoopField)
		{
		GuiControl, 1:+Background%i%, LV%A_Index%
		LV%A_Index% := i
		}
if !sws%r%
	SetTimer, sss, -1
}
;================================================================
action1:
;================================================================
action=
if TBIdx=3
	action = selfolder
if TBIdx=6
	action = save
if TBIdx=7
	action = saveas
if TBIdx=18
	action = zoom
if TBIdx=22
	action = options
if TBIdx=23
	action = about
if TBIdx=24
	action = saveandexit
SetTimer, %action%, -1
if !action
	msgbox, Clicked button %TBIdx% for action %TBCtrl%
return
;================================================================
seltask:
;================================================================
row := line
Tooltip						; avoid having the tooltip "stick" to screen after selecting an action
if tidx=1
	{
	btnclr := A_GuiControl
	if row in 5,6,7,14,16
		goto PickColor
	}
else if tidx=2
	{
	if row in 1,7,8
		goto selbmp
	}
return
;================================================================
selTransClr:
;================================================================
btnclr := A_GuiControl
row=
;================================================================
PickColor:
;================================================================
if (A_GuiEvent != "Normal")
	return
i := ChgColor(SwColor(%btnclr%), "BR", 1)
if ErrorLevel
	return
%btnclr% := i
GuiControl, 1:+Background%i%, %btnclr%
if !row
	goto ToggleCount
GuiControl, 1:, edt%row%, % (i>>16 & 0xFF) " " (i>>8 & 0xFF) " " (i & 0xFF)
Gui, Submit, NoHide
if !sws%row%
	goto sss
Return
;================================================================
selbmp:
;================================================================
bmppath := bmppath ? bmppath : tfpath "\" t1
FileSelectFile, i, 3, %bmppath%, Select bitmap file:, Bitmap files (*.bmp)
if (!i OR ErrorLevel)
	return
GuiControl, 1:, edt%row%, %i%
Gui, 1:Submit, NoHide
return
;================================================================
saveas:
;================================================================
FileSelectFile, i, S24, %inifile%, Save theme ini file as:, RP theme (*.ini)
if (!i OR ErrorLevel)
	return
Sleep, 500			; try to give the stupid Win7 time to "think" and attach the fuckin' extension
Loop, %i%									; check for missing/wrong extension
	if (A_LoopFileExt != "ini")
		i .= ".ini"
inifile := i
IfNotExist, %inifile%
	FileAppend, `;Please note - file content is critical`n`n, %inifile%
goto saveth
;================================================================
save:
;================================================================
if (!inifile OR inifile=A_ScriptDir "\no skin.ini")
	{
	MsgBox, 0x2021, RPTE,
		(LTrim
		Sorry, there is no skin ini loaded!

		If this is meant for a new theme,
		click OK to choose the destination
		otherwise Cancel to abort saving.
		)
	IfMsgBox OK
		goto saveas
	return
	}
SplitPath, inifile,fn, fdir
inifile := fdir "\mod_" fn
;================================================================
saveth:
;================================================================
Loop, Parse, sections, CSV
	{
	idx := A_Index
	ctvi := tvi%idx%
	cs := A_LoopField
	items := cs="Global" ? inilist1 : inilist2
	if !ArrayGet(theme, ctvi "_C" idx)				; find out if section exists and has items
		continue
	if (cs="StartButton")
		{
		gosub subsave
		if i := TV_GetChild(tvi%idx%)
			{
			gosub chldsave
			Loop
				{
				if !i := TV_GetNext(i)
					break
				gosub chldsave
				}
			}
		}
	else gosub subsave
	}
MsgBox, 0x2040, RPTE, File saved as`n%inifile%.
return
;================================================================
chldsave:
;================================================================
ctvi := i
TV_GetText(lang, ctvi)
s := FindLang(lang, "L")+0
cs := "StartButton." s
;================================================================
subsave:
;================================================================
Loop, Parse, items, CSV
	{
	v := ArrayGet(theme, ctvi "_N" A_Index)				; Get value from New array
	sw := ArrayGet(theme, ctvi "_S" A_Index)				; Get switch
	if (sw && v!="")
		IniWrite, %v%, %inifile%, %cs%, %A_LoopField%
	else IniDelete, %inifile%, %cs%, %A_LoopField%
	}
return
;================================================================
setTBicons:
setTBiconm:
setTBiconl:
;================================================================
StringRight, i, A_ThisLabel, 1
if (i=pref_isize)
	return
pref_isize := i
Menu,MenuLevel6, % (pref_isize="s" ? "Check" : "Uncheck"), &small`t(16px)
Menu,MenuLevel6, % (pref_isize="m" ? "Check" : "Uncheck"), &medium`t(24px)
Menu,MenuLevel6, % (pref_isize="l" ? "Check" : "Uncheck"), &large`t(32px)
TB_SetIL(hTB1, "I0|" hILt%pref_isize%, 5)			; add D,H,P0 lists (Disabled, Hot, Pressed) space-separated

TB_Set(hTB1, "b" 0x18|(0x18<<16) " d" 0x4|(0x8<<16))
b2h := TB_Get(hTB1, "bH")						; initial toolbar height
RB_Set(hRB1, band2, "ic|" b2h "|200|" b2h "|200")
;RB_Size(hRB1)
if !DllCall("DeleteObject", Ptr, hBack) && debug
	LogFile("Failure deleting handle: " hBack, A_LineNumber, A_LineFile, A_ThisLabel, A_ThisFunc
	, "Cannot delete hBack in skin chooser", debugout)
ControlGetPos,,, w,,, ahk_id %hRB1%
h := RB_GetBand(hRB1, band2, "hr")
hBack := ResizeBmp(hSkin, w, h)
RB_Set(hRB1, band1, "bk" hBack)
RB_Set(hRB1, band2, "bk" hBack)
return
;================================================================
#include mod\settingsGUI.ahk
;================================================================
options:
if IsLabel("Sshow")
	goto Sshow
testlabel:
;================================================================
msgbox, To be done...
return
;================================================================
;	SKIN SELECTOR
;================================================================
ChSkin:
Loop, %skincount%
	{
	GuiControl, 3:-Border, image%A_Index%
	hDSkin%A_Index% := ILC_FitBmp(hSkinImg%A_Index%, skin, A_Index)
	if debug
		LogFile("Skin handle " A_Index ": " hDSkin%A_Index%, A_LineNumber, A_LineFile, A_ThisLabel, A_ThisFunc
		, "Add skin " A_Index " to skin chooser (hDSkin" A_Index ")", debugout)
	}
GuiControl, 3:Move, Static1, % "x" 4+65*(cSkin-1-8*((cSkin-1)//8)) " y" 4+65*((cSkin-1)//8)
GuiControl, 3:Move, Static2, % "x" 4+65*(cSkin-1-8*((cSkin-1)//8)) " y" 4+65*((cSkin-1)//8)
Gui, 3:Show, AutoSize Center
return
;================================================================
selskinimg:
;================================================================
StringReplace, cSkin, A_GuiControl, image,
if !DllCall("DeleteObject", Ptr, hSkin) && debug
	LogFile("Failure deleting handle: " hSkin, A_LineNumber, A_LineFile, A_ThisLabel, A_ThisFunc
	, "Cannot delete hSkin in skin chooser", debugout)
if !DllCall("DeleteObject", Ptr, hBack) && debug
	LogFile("Failure deleting handle: " hBack, A_LineNumber, A_LineFile, A_ThisLabel, A_ThisFunc
	, "Cannot delete hBack in skin chooser", debugout)
hSkin := GetBmp(cSkin, skinsz, skin, 100, 0)
if debug
	LogFile("Skin handle: " hSkin, A_LineNumber, A_LineFile, A_ThisLabel, A_ThisFunc
	, "Create new skin (hSkin)", debugout)
skincolor := GetPixelColor(hSkin, 0, 1)
menucolor := SwColor(skincolor)
;tc2 := skincolor
;tc1 := tc2 & 0x007F7F7F
Gui, 1:Color, %menucolor%, %ctrlcolor%
Sleep(1)
i := DllCall("IsWindowEnabled", Ptr, hOpaq)
GuiControl, % "1:" (i ? "Disable" : "Enable"), opaq
GuiControl, % "1:" (i ? "Enable" : "Disable"), opaq
;Gui, 2:Color, %menucolor%, %ctrlcolor%
Gui, 4:Color, %menucolor%, %ctrlcolor%
Menu, Tray, Color, %menucolor%
Menu, Main, Color, %menucolor%
Menu, MenuLevel6, Color, %menucolor%
/*
if hBrMenu
	if !DllCall("DeleteObject", Ptr, hBrMenu) && debug
		LogFile("Failure deleting brush handle: " hBrMenu, A_LineNumber, A_LineFile, A_ThisLabel
		, A_ThisFunc, "Cannot delete hBrMenu in skin chooser", debugout)
*/
ControlGetPos,,, w,,, ahk_id %hRB1%
h=0
r := RB_Get(hRB1, "r")
c := RB_Get(hRB1, "c")
Loop, %c%
	if ((nh%A_Index% := RB_GetBand(hRB1, A_Index, "hr")) > h)
		h := nh%A_Index%
hBack := ResizeBmp(hSkin, w, h)
if debug
	LogFile("Skin handle: " hBack, A_LineNumber, A_LineFile, A_ThisLabel, A_ThisFunc
	, "Create new rebar skin (hBack)", debugout)
RB_Set(hRB1, band1, "bk" hBack)
RB_Set(hRB1, band2, "bk" hBack)
tSkin=1		; if user changed skin, assume they want a fixed one so disable random skin choosing
/*
hBrMenu := MenuSkin(hMainMenu, hBack, 0)
if debug
	LogFile("Brush handle: " hBrMenu, A_LineNumber, A_LineFile, A_ThisLabel, A_ThisFunc
	, "Create new menu brush (hBrMenu)", debugout)
*/
;================================================================
3GuiEscape:
Gui, 3:Hide
return		; new versions of func_ImageList delete the old image handle automatically
;================================================================
#include *i mod\aboutGUI.ahk
about:
;================================================================
i := "about2"
if IsLabel(i)
	goto %i%
else
MsgBox, 0x2040, About %appname%,
(
%comment%
by %author%
version %version% %releaseT%
released %releaseD%
contact: %authormail%

This application is open-source,
created in AutoHotkey 1.0.48.05
under Windows 98SE.
Source code also compatible with AHK_L.

Credit goes to Tihiy, author of the
Revolutions Pack, also for descriptions.
Original docs: http://tihiy.net/uber/
Main pack: http://tihiy.net/files/RP9.exe
Extra: http://tihiy.net/files/RP9Updates.exe

Icons kindly provided by Mark James at:
http://www.famfamfam.com/lab/icons/silk/
Thank you CharlesF at MSFN for testing.

EVERYTHING should be free !
)
return
;================================================================
;	FUNCTIONS
;================================================================
msmove(wP, lP, msg, hwnd)
{
Global
Local mx, my, winid, ctrl, ctrl2, idx
Static last, ttmsg, lvs="5,6,7,14,16"
MouseGetPos, mx, my, winid, ctrl, 2
MouseGetPos,,,, ctrl2
if (winid=hGui3)
	ImgHover(hGui3, ctrl2)
if (winid != hMain OR tab != 1)
	{
	if ctrl in %hFTC%
		DllCall("SetCursor", Ptr, hCursH)
	SetTimer, showtt, Off
	Tooltip
	return
	}
WP2CP(ctrl, mx, my, 1)						; do this in view of possible theme changes
tidx := !tvi OR tvi=tvbase ? 0 : tvi!=tvi1 ? 2 : 1	; section type, if any (0-none, 1-global, 2-other)
maxy := _l-1+19*(!tidx ? 0 : tidx>1 ? 12 : 20)	; max Y for respective lines
line := (mx>=tvw && mx<=guimw && my>=_l && my<=maxy) ? 1+(my-_l)//19 : 0
if ctrl in %lblH%
	{
	DllCall("SetCursor", Ptr, hCursL)
	ttmsg := tidx=1 ? descG%line% : descC%line%
	ttdelay := -pref_ttdelay
	}
else if ctrl in %clrH%
	{
	DllCall("SetCursor", Ptr, hCursH)
	ttmsg := descBc
	ttdelay := -pref_ttdelay
	}
else if ctrl in %selH%
	{
	DllCall("SetCursor", Ptr, hCursH)
	ttmsg := descBs
	ttdelay := -pref_ttdelay
	}
else if ctrl in %dftH%
	{
	DllCall("SetCursor", Ptr, hCursH)
	ttmsg := descBd
	ttdelay := -pref_ttdelay
	}
else if ctrl in %oriH%
	{
	DllCall("SetCursor", Ptr, hCursH)
	ttmsg := descBo
	ttdelay := -pref_ttdelay
	}
else if ctrl in %hBTask%
	{
	DllCall("SetCursor", Ptr, hCursH)
	ttmsg := descBb
	ttdelay := -pref_ttdelay
	}
else if (ctrl="")
	{
	ttmsg := !tidx ? "" : tidx=1 ? descG0 : descC0
	ttdelay := -4*pref_ttdelay
	}
else
	{
	ttmsg := ""
	ttdelay := -pref_ttdelay
	}
if (ctrl != last)
	{
	Tooltip
	SetTimer, showtt, %ttdelay%
	}
if (line && tidx=1)
	{
	Loop, Parse, lvs, CSV
		if (line=A_LoopField)
			GuiControl, Show, LV%A_Index%
		else GuiControl, Hide, LV%A_Index%
	}
else if (line && tidx=2)
	{
	if line in 1,7,8
		{
		GuiControl, Move, btntask, % "y" _l+19*(line-1)
		GuiControl, Show, btntask
		}
	else GuiControl, Hide, btntask
	}
else
	{
	GuiControl, Hide, btntask
	Loop, 5
		GuiControl, Hide, LV%A_Index%
	}
last := ctrl
return

showtt:
Tooltip, %ttmsg%
SetTimer, hidett, -%pref_ttshow%
return

hidett:
Tooltip
return
}
;================================================================
cmnd(wP, lP, msg, hwnd)
;================================================================
{
Global
Local action, i
action := wP & 0xFFFF
action=%action%
if (lP=hTB1)
	{
	TBIdx := action
	SetTimer, action1, -1
	}
else if (lP=hTB2)
	{
;	msgbox, Button #%action% was pressed.
	if action=0xFFFF
		{
		TB2Idx := action
;		SetTimer, action2, -1
		return
		}
	TB_BtnSet(hTB2, action, "s0x8")
	}
}
;================================================================
notif(wP, lP, msg, hwnd2)
;================================================================
{
Global
Static hwnd, code, cmdid, rect, w1, h1, coord="x1,y1,x2,y2"
hwnd := NumGet(lP+0, 0, Ptr)
code := NumGet(lP+0, 8, "UInt")
cmdid := NumGet(lP+0, 12, "UInt")
if code=0xFFFFFD3D			; TBN_QUERYDELETE
	{
	if (hwnd=hTB1)
		{
		}
	return TRUE
	}
else if code=0xFFFFFD3E		; TBN_QUERYINSERT
	{
	if (hwnd=hTB1)
		{
		}
	return TRUE
	}
else if code=0xFFFFFD3A		; TBN_DROPDOWN
	{
;	return TRUE
	}
else if code=0xFFFFFD37		; TBN_HOTITEMCHANGE
	{
;	return TRUE
	}
/*
;else if code=0xFFFFFDD8		; TCN_SELCHANGING
else if code=0xFFFFFDD9		; TCN_SELCHANGE
	{
if tbvis
	{
	DllCall("InvalidateRect", Ptr, hThemes, Ptr, 0, "UInt", 1)
	DllCall("InvalidateRect", Ptr, hTB1, Ptr, 0, "UInt", 1)
	DllCall("UpdateWindow", Ptr, hThemes)
	DllCall("UpdateWindow", Ptr, hTB1)
	}

	}
*/
else if (hwnd=hRB1)
	{
	 if code=0xFFFFFCBE		; RBN_AUTOSIZE
		{
		}
	else if code=0xFFFFFCB7	; RBN_CHEVRONPUSHED
		{
		msgbox, Chevron command #%cmdid%
		}
;	else if code=0xFFFFFCB9	; RBN_CHILDSIZE
;	else if code=0xFFFFFCBC	; RBN_ENDDRAG
	else if code=0xFFFFFCC1	; RBN_HEIGHTCHANGE
		{
		VarSetCapacity(rect, 16, 0)
		DllCall("GetClientRect", Ptr, hMain, Ptr, &rect)
		Loop, Parse, coord, CSV
			%A_LoopField% := NumGet(rect, 4*(A_Index-1), "Int")
		w1 := x2-x1, h1 := y2-y1
		SendMessage, 0x5, 0, w1|(h1<<16),, ahk_id %hMain%
;		DllCall("MoveWindow", Ptr, hMain, "Int", x, "Int", y, "Int", w, "Int", h, "UInt", TRUE)
;		return TRUE
		}
	}
;SetFormat, Integer, %ofi%
}
;================================================================
WProc(hwnd, msg, wP, lP)
{
Global
if (msg=0x14 && tbvis)	; WM_ERASEBKGND
	{
/*
;	ControlGetPos, RBpX, RBpY, RBpW, RBpH,, ahk_id %hRB1%
	VarSetCapacity(RC, 16, 0)
	hRDC := DllCall("GetDC", Ptr, hRB1)
	DllCall("GetClipBox", Ptr, hRDC, Ptr, &RC)
	DllCall("ReleaseDC", Ptr, hRB1, Ptr, hRDC)
;	DllCall("ValidateRect", Ptr, hMain, Ptr, &RC)
	ControlGetPos, RBpX, RBpY, RBpW, RBpH,, ahk_id %hThemes%
;	DllCall("ExcludeClipRect", Ptr, wP, "Int", RBpX-5, "Int", RBpY-45, "Int", RBpX+RBpW, "Int", RBpY+RBpH)
	ControlGetPos, RBpX, RBpY, RBpW, RBpH,, ahk_id %hTB1%
	DllCall("ExcludeClipRect", Ptr, wP, "Int", RBpX-5, "Int", RBpY-45, "Int", RBpX+RBpW, "Int", RBpY+RBpH)
;tooltip, %RBpX%x%RBpY% %RBpW%x%RBpH%
	DllCall("InvalidateRect", Ptr, hThemes, Ptr, 0, "UInt", 1)
	DllCall("InvalidateRect", Ptr, hTB1, Ptr, 0, "UInt", 1)
*/
	}
return DllCall("CallWindowProc" AW, Ptr, oWP, Ptr, hwnd, "UInt", msg, "UInt", wP, Ptr, lP)
}
;================================================================
;	HOVER EFFECT in GRID / SKIN SELECTOR
;================================================================
ImgHover(hwnd, ctrl)
{
Global hCursH, Ptr
StringReplace, ctrl, ctrl, Static,,
if ctrl < 3
	return
DllCall("SetCursor", Ptr, hCursH)
ControlGetPos, X, Y,,, Static1, ahk_id %hwnd%
ControlGetPos, cX, cY,,, Static%ctrl%, ahk_id %hwnd%
if (X != cX-1) OR (Y != cY-1)
	{
	GuiControl, 3:Hide, Static1
	ControlMove, Static1, % cX-1, cY-1,,, ahk_id %hwnd%
	GuiControl, 3:Show, Static1
	}
}
;================================================================
easymove(hwnd, msg, wP, lP)
;================================================================
{
Global
ofi := A_FormatInteger
SetFormat, Integer, D
hwnd+=0
SetFormat, Integer, %ofi%
if msg=0x201
	wP:=2, msg := 0xA1
return DllCall("CallWindowProc" AW, Ptr, oWP%hwnd%, Ptr, hwnd, "UInt", msg, "UInt", wP, Ptr, lP)
}
;================================================================
PathCombine(p1, p2)
;================================================================
{
Global AW
SplitPath, p1,, pd
VarSetCapacity(p, 260*2**(A_IsUnicode=TRUE), 0)
DllCall("shlwapi\PathCombine" AW, "Str", p, "Str", pd, "Str", p2)
return p, VarSetCapacity(p, -1)
}
;================================================================
isPathRelative(p)
;================================================================
{
Global
return DllCall("shlwapi\PathIsRelative" AW, "Str", p)
}
;================================================================
LoadBmp(bFile, ByRef bw, ByRef bh, ByRef bpp, pma="")
;================================================================
{
Global Ptr, AW
if !hB := DllCall("LoadImage" AW, Ptr, 0, "Str", bFile, "UInt", 0, "Int", 0, "Int", 0, "UInt", 0x2010, Ptr)
	return
VarSetCapacity(buf, sz := 24, 0)	; BITMAP struct (DIBSECTION is 84, not needed)
DllCall("GetObject", Ptr, hB, "UInt", sz, Ptr, &buf)
bw := NumGet(buf, 4, "Int")
bh := NumGet(buf, 8, "Int")
bpp := NumGet(buf, 18, "UShort")
if (bpp>=32 && !pma)
	{
	ba := NumGet(buf, 20, Ptr)
	PremultAlpha(ba, bw*bh)
	}
return hB
}
;================================================================
PremultAlpha(ba, raw)
;================================================================
{
Loop, %raw%
	{
	a := NumGet(ba+0, 4*(A_Index-1)+3, "UChar")
	NumPut(NumGet(ba+0, 4*(A_Index-1)+0, "UChar")*a/255, ba+0, 4*(A_Index-1)+0, "Char")
	NumPut(NumGet(ba+0, 4*(A_Index-1)+1, "UChar")*a/255, ba+0, 4*(A_Index-1)+1, "Char")
	NumPut(NumGet(ba+0, 4*(A_Index-1)+2, "UChar")*a/255, ba+0, 4*(A_Index-1)+2, "Char")
	}
}
;================================================================
;	INCLUDES
;================================================================
#include func\func_Array.ahk
#include func\func_Brush.ahk
#include func\func_ChgColor.ahk
#include func\func_ComboBoxEx.ahk
#include func\func_DllGetVersion.ahk
#include func\func_DrawTransBmp.ahk
#include func\func_FastIni.ahk
#include func\func_FindLang.ahk
#include func\func_GetHwnd.ahk
#include func\func_GetOSver.ahk
#include func\func_ImageList.ahk
#include func\func_IPAddr.ahk
#include func\func_LibraryOp.ahk
#include func\func_LogFile.ahk
#include func\func_LVExtra.ahk
#include func\func_Rebar.ahk
#include func\func_Toolbar.ahk
#include func\func_Zoom.ahk
